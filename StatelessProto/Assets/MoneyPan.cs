using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MoneyPan : MonoBehaviour
{

    [SerializeField] TMP_Text mText;
    [SerializeField] float kDemp;
    float prepSum;
    float curSum;

    void Start()
    {
        Glob.DoTick += DoTick;
    }

    void Update()
    {
        curSum = Mathf.Lerp(prepSum, curSum, kDemp);
        mText.text = curSum.ToString("n0");
    }
    void DoTick()
    {
        prepSum = Glob.totalMoney;
    }
}   
