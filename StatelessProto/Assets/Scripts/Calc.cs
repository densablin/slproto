using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calc : MonoBehaviour
{
    [Header("for Tens")]
    [Tooltip("���� ����� ������ Agr ������ �������")]
    [SerializeField] float tensAgrLowTresh = .5f; // ������ ������� �������
    [Tooltip("���� ����� ������ Agr ��������� ������")]
    [SerializeField] float tensAgrHiTresh = .85f; // ������� ������� �������
    [Tooltip("��� ������ Agr � ����� �������� Tens")]
    [SerializeField] float tensAgrWeight = .5f; // ��������� ������ ����� 
    [Tooltip("��� �������������� ������� � ����� Tens")]
    [SerializeField] float tensRelOpositWeight = .3f;
    [Tooltip("�������, � �������� ��������� ��� �������������� �������� � ����� Tens")]
    [SerializeField] float tensRelSolidTresh = 1.2f;
    [Tooltip("��� �������������� �������� � ����� Tens")]
    [SerializeField] float tensRelSolidWeight = .5f;
    [Tooltip("������� ������ ��� ���������� ������� ��������")]
    [SerializeField] float tensArmorLevelPermiss = .5f;
    [Tooltip("��� ���������� ������ ��� ���������� ����������� ��� ��� ������")]
    [SerializeField] int minCellforFight = 3;

    [Header("for Transfers")]
    [Tooltip("���� ����� ������ ������ ��������� ������ � ��������")]
    [SerializeField] float tensTreshToTrans = .9f;
    [Tooltip("��� ����� ������� ������� � �������� �����������")]
    [SerializeField] float chanceNhWeight = .2f;
    [Tooltip("��� ����� ������������� �������� � �������� �����������")]
    [SerializeField] float chanceAgrDifWeight = 3f;
    [Tooltip("��� ����� ������������� ������������ � �������� �����������")]
    [SerializeField] float chanceArmorDifWeight = 1f;

    [Tooltip("���� ������ � ���������� �������� ���")]
    [SerializeField] float armorBonusPerTrans = .00f;
    [Tooltip("���� ������ � ���������� ��������� ���")]
    [SerializeField] float armorLossPerTrans = -.01f;
    [Tooltip("���� �������� ���.������ � ���������� ���")]
    [SerializeField] float armorChancePerTrans = .1f;
    [Tooltip("��������� ���. ���� ������ � ���������� ���")]
    [SerializeField] float armorBonusOnChance = .1f;
    [Tooltip("���� �������� � ���������� �������� ���")]
    [SerializeField] float agrBonusPerTrans = .00f;
    [Tooltip("���� �������� � ���������� ��������� ���")]
    [SerializeField] float agrLossPerTrans = -0.01f;
    [Tooltip("��������� ���. ���� �������� �� ���������� ��� (�����)")]
    [SerializeField] float agrDose = .01f;


    [Header("Common Normals")]
    public CProps normPropsCom; //  ���������� ������� ����������, �� ������� ������������� ��� ��������

    [Header("Noise Section")]
    public float noiseK = .1f; // ����������� ��������� ����� �� Data � ������ ������
    // ��������� ���������� ���� �� ���
    public CProps noiseAmpPropsCom;
    // ������� ������������� ���� � ��������, ������������ �����
    public float noiseLimRel = .3f;
    public float noiseLimCrown = .3f;
    public float noiseLimAgr = .2f;
    public float noiseLimArmor = .5f;
    public float noiseLimEcon = .2f;


    [Header("Demp Section")]
    public float dempK = .1f; // ����������� ��������� ������� �������� �� Data � ������ ������
    // ����� �������� �� ���
    public CProps dempKPropsCom;


    [Header("Scales Agent==>Calc")]

    // ����������� ���������� � �������� ��������� �� �������
    [SerializeField] float scaleKRel = .4f; // ��� �������
    [SerializeField] float scaleKAgr = .4f; // ��� �������
    [SerializeField] float scaleKCrown = .2f; // ��� ����������

    [Header("Others")]
    [Tooltip("���� ����� ������ ����� ��������� ������ � �������")]
    [SerializeField] float ArmorTreshForAccept = 0.2f;
    [Tooltip("���� ����� ������ ������������ ���������� ��� ����� �������")]
    [SerializeField] float crownHoldTresh = .75f;
    [Tooltip("���������� �������� �������� ��� ��������")]
    [SerializeField] float agrCDIncPerTick = .01f;
    [Tooltip("���������� ��������� �������������� �������")]
    [SerializeField] float supRelK = .1f;


    public float IntegrateK = .1f; // ����������� ��� �������������� ����� ���������� ������ � ����

    private void Awake()
    {
        Glob.Calc = this;
        Glob.DoTick += CalcTick;
    }
    public float CalcTens(Country cnA, Country cnB)
    {
        // ������������� ��-������ ������� �� ������������� ����� ����� (��������)
        float k = (Mathf.InverseLerp(tensAgrLowTresh, tensAgrHiTresh, cnA.prop.agr)
                    + Mathf.InverseLerp(tensAgrLowTresh, tensAgrHiTresh, cnB.prop.agr))
                    * tensAgrWeight;
        // ��������� �������������� ����������� ����� ��������
        k += CalcRelOpposites(cnA.prop, cnB.prop) * tensRelOpositWeight;

        // ��������� �������������� �������� ����� �������� (��������)
        k -= Mathf.InverseLerp(tensRelSolidTresh, 2, CalcRelSolidarity(cnA.prop, cnB.prop)) * tensRelSolidWeight;

        k = Mathf.Clamp01(k);
        return k;
    }

    public bool CalcAfterTransChance(HexCell hc) // ��������, ����� ����� ������ 
                                                 // �������� �� ������ � �������� ����� ���
    {
        float otherV = .5f;

        // ������ ������� ���������� ��������: c������ ������� 
        // ������ ���� ������� ����
        int n = hc.HowManyNHCells(hc.ocupCounID);
        if (n < 2 && Glob.Countries[hc.ocupCounID].myCells.Count > Glob.Countries[hc.ocupCounID].areaMin)
        {
            // �� ���� ������ ������ �� ������� -- ��� �� ��������
            return false;
        }

        // ����� ������� ������� � ����������� ����������
        float k = Mathf.InverseLerp(2, 6, n);
        otherV += k * chanceNhWeight;

        // ����� ������� � �������������
        k = Glob.Countries[hc.ocupCounID].prop.agr
            - Glob.Countries[hc.hostCounID].prop.agr;
        otherV += k * chanceAgrDifWeight;

        // ������� ������������� � ������ �������������
        k = Glob.Countries[hc.ocupCounID].prop.armor
            - Glob.Countries[hc.hostCounID].prop.armor;
        otherV += k * chanceArmorDifWeight;

        // ��� �������� ������
        float v = Random.Range(0f, 1f);
        return (v < otherV);
    }
    public void FindCellsToTrans() // ���� ������, ������� ����� (� ����� ��) �������� � ���
    {
        int dim = 1; // ������� ������ �� ������ ������ ������ ��� ���� �� ���� ���

        for (int i = 0; i < Glob.Countries.Length; i++)
        {
            if (!Glob.Countries[i].isActive) continue;



            for (int j = 0; j < Glob.Countries.Length; j++) // ��� ���� ��������� � ������������,
                                                            // �.�. ������ � ����� ������
            {
                if (i == j) continue;
                if (!Glob.Countries[j].isActive) continue;

                if (Glob.RelateMx[i, j].Tens >= tensTreshToTrans)
                {
                    // ��� ������ ������ ������ � ����� ����� ������ ����
                    // ������ �� ������, ����, ���� ������ ���������
                    if (Glob.Countries[i].prop.armor < tensArmorLevelPermiss
                        && Glob.Countries[i].transCellCount < minCellforFight) continue;
                    if (Glob.Countries[j].prop.armor < tensArmorLevelPermiss
                        && Glob.Countries[j].transCellCount < minCellforFight) continue;

                    for (int t = 0; t < dim; t++)
                    {
                        HexCell hc = Glob.MField.GetRandomFrontirCell(i, j);
                        if (hc != null)
                        {
                            // � ���� �� ��� �������� ��� ��� ��� ����� � ������???
                            // �����: ����� � ������

                            HexCell otherHc = hc.GetRandomNHCell(j);

                            if (otherHc != null)
                            {
                                if (!otherHc.isInTransit)
                                {
                                    if (Glob.Countries[otherHc.myCountryID].isActive)
                                    {
                                        // ���� ��� ��, ��
                                        // ������� ������, ����� ����������� ���������� � ��������� �������
                                        // �������� �� ���� �������
                                        otherHc.StartTransit(i);
                                    }
                                    else
                                    {
                                        print("�������� ��������� ������ ���������� ������, ������???"
                                            + " i=" + i + " other cn=" + otherHc.myCountryID);
                                    }
                                }
                            }
                        }
                    }

                }

            }
        }

        // return hList;
    }

    public void AcceptTransitResults(int winnerCntId, int looserCntId)
    {
        // ������ ���� ������ ��� ���������� � ����������� �������
        Glob.Countries[winnerCntId].preProp.armor += armorBonusPerTrans;
        Glob.Countries[looserCntId].preProp.armor += armorLossPerTrans;

        // ������ ���� �������� ��� ���������� � ����������� �������
        Glob.Countries[winnerCntId].preProp.agr += agrBonusPerTrans;
        Glob.Countries[looserCntId].preProp.agr += agrLossPerTrans;

        // ���� �������� �������� ������
        if (Random.Range(0f, 1f) < armorChancePerTrans)
        {
            Glob.Countries[winnerCntId].preProp.armor += armorBonusOnChance;
            //Glob.Countries[looserCntId].preProp.armor -= armorBonusOnChance;

        }

        if (Random.Range(0f, 1f) < armorChancePerTrans)
        {
            Glob.Countries[looserCntId].preProp.armor += armorBonusOnChance;
            //Glob.Countries[winnerCntId].preProp.armor -= armorBonusOnChance;

        }

        // ����� ���������� ��� ����� ��������
        if (Glob.Countries[looserCntId].myCells.Count < Glob.Countries[looserCntId].areaMin
            && Glob.Countries[looserCntId].agrCoolDown > .9)
        {
            print("cn=" + looserCntId + " Dosed!!!!");
            Glob.Countries[looserCntId].agrCoolDown = 0;
            Glob.Countries[looserCntId].preProp.agr += agrDose;
        }


    }
    public Vector3 SupportLeaderRel(float relA, float relB, float relC, float range)
    {
        Vector3 ret = new Vector3(relA, relB, relC);

        if (relA == Mathf.Max(relA, relB, relC)) ret[0] += range;
        if (relB == Mathf.Max(relA, relB, relC)) ret[1] += range;
        if (relC == Mathf.Max(relA, relB, relC)) ret[2] += range;

        return ret;
    }
    public float CalcRelOpposites(CProps cnt1, CProps cnt2)
    {
        float dA = cnt1.relA - cnt2.relA;
        float dB = cnt1.relB - cnt2.relB;
        float dC = cnt1.relC - cnt2.relC;
        return Mathf.Max(dA, dB, dC) - Mathf.Min(dA, dB, dC);
    }
    public float CalcRelSolidarity(CProps cnt1, CProps cnt2)
    {
        float dA = cnt1.relA + cnt2.relA;
        float dB = cnt1.relB + cnt2.relB;
        float dC = cnt1.relC + cnt2.relC;
        return Mathf.Max(dA, dB, dC);
    }
    public void AddAgentProps(Agent ag, int countID)
    {
        Glob.Countries[countID].preProp =
            SumProps(
                Glob.Countries[countID].preProp,
                ScaleAgentProps(ag.prop)
                );
    }
    public CProps ScaleAgentProps(CProps a) // ��������� ������ � �������� � ��������, ����������� � �������
    {
        CProps ret = new CProps
        {
            relA = a.relA * scaleKRel,
            relB = a.relB * scaleKRel,
            relC = a.relC * scaleKRel,
            agr = a.agr * scaleKAgr,
            crown = a.crown * scaleKCrown,
            armor = a.armor * 1f,
            econ = a.econ * 1f,
        };

        return ret;
    }
    void CalcTick()
    {
        // ��� ������ ������
        foreach (var cn in Glob.Countries)
        {
            if (!cn.isActive) continue;

            // ������� ����, ��� ������ ������ ��������������� � preProp
            // ����� ����� ���� ������� ������ �������

            // �������� ������ ��� ��������� ��������
            cn.bufAdds = InitZeroProps();
            cn.bufNorms = InitBufferProps(normPropsCom);

            // ��� ����������� ������ �������� �� ����� �����
            // ���������� � ����� ���������� ����������
            // ���������� � ����� ���� ����������� �����, � ������� ����� ����� ��������������

            if (Glob.Cells[cn.capitalCellId].myCountryID == cn.cnID)
            {
                // ���� ������� �� �����, ���������� ������
                if (cn.preProp.crown > crownHoldTresh)
                {
                    cn.preProp.crown += cn.dempProp.crown;
                }
            }

            // ������������ �������������� ���������
            float supRange = cn.prop.crown * supRelK;
            Vector3 newRel = SupportLeaderRel(cn.preProp.relA, cn.preProp.relB, cn.preProp.relC, supRange);
            cn.preProp.relA = newRel[0];
            cn.preProp.relB = newRel[1];
            cn.preProp.relC = newRel[2];

            // ���������, �������� ������� ������
            cn.preProp = SumProps(cn.preProp, cn.bufAdds);

            // ��������� ���
            //cn.preProp = GenPropNoise(cn.preProp, cn.noiseProp, cn.bufNorms);
            cn.preProp = GenPropNoise(cn.preProp, cn.noiseProp);

            // ���������� � ������, ������� ����� � ������ ����� ��������
            cn.preProp = StepDempProp(cn.preProp, cn.bufNorms, cn.dempProp);

            // ������������, ���� ��� �����:
            // �������� ��������� � 1
            cn.preProp = NormalizeRels(cn.preProp);

            // ������������ ������ � �����
            cn.preProp = ClampProps(cn.preProp);

            // ����������� ����� �������� � �������
            IntegrateProps(cn.cnID);

            // ��������� �������� ��� argCoolDown
            cn.agrCoolDown += agrCDIncPerTick;
            cn.agrCoolDown = Mathf.Clamp01(cn.agrCoolDown);
        }


        // ��� ������� ����� ���������� �� ����� ���������� ��� ���� �����
        CalcRelateMx();

        // � ��������� ��� ���� � ����� � ������ ������������
        Glob.MField.UpViewAllFrontCells();

        // ��� �������� ������ ��� ��������
        FindCellsToTrans();

    }
    public static CProps InitBufferProps(CProps inProps)
    {
        // ����� ��������, ��� ������ ���� ��������, 
        // �.�. ��������� ���-�� ���?
        CProps ret = new CProps()
        {
            relA = inProps.relA,
            relB = inProps.relB,
            relC = inProps.relC,
            crown = inProps.crown,
            agr = inProps.agr,
            armor = inProps.armor,
            econ = inProps.econ,
        };

        return ret;
    }
    public static CProps InitZeroProps()
    {
        CProps ret = new CProps()
        {
            relA = 0,
            relB = 0,
            relC = 0,
            crown = 0,
            agr = 0,
            armor = 0,
            econ = 0,
        };

        return ret;
    }
    public static CProps ConvertEffectToProps(effectData[] eff)
    {
        CProps ret = InitZeroProps();

        return IncludeEffectToProps(eff, ret);
    }

    public static CProps IncludeEffectToProps(effectData[] eff, CProps inProps)
    {
        CProps ret = inProps;

        if (eff != null)
        {
            foreach (var e in eff)
            {
                switch (e.var)
                {
                    case "relA": ret.relA = e.val; break;
                    case "relB": ret.relB = e.val; break;
                    case "relC": ret.relC = e.val; break;
                    case "agr": ret.agr = e.val; break;
                    case "crown": ret.crown = e.val; break;
                    case "armor": ret.armor = e.val; break;
                    case "econ": ret.econ = e.val; break;
                }
            }
        }
        return ret;
    }

    public CProps SumProps(CProps a, CProps b)
    {
        CProps ret = new CProps
        {
            relA = a.relA + b.relA,
            relB = a.relB + b.relB,
            relC = a.relC + b.relC,
            agr = a.agr + b.agr,
            crown = a.crown + b.crown,
            armor = a.armor + b.armor,
            econ = a.econ + b.econ
        };

        return ret;
    }
    public CProps LerpProps(CProps a, CProps b, float t)
    {
        CProps ret = new CProps
        {
            relA = Mathf.Lerp(a.relA, b.relA, t),
            relB = Mathf.Lerp(a.relB, b.relB, t),
            relC = Mathf.Lerp(a.relC, b.relC, t),
            agr = Mathf.Lerp(a.agr, b.agr, t),
            crown = Mathf.Lerp(a.crown, b.crown, t),
            armor = Mathf.Lerp(a.armor, b.armor, t),
            econ = Mathf.Lerp(a.econ, b.econ, t),
        };

        return ret;
    }

    public CProps ClampProps(CProps a)
    {
        CProps ret = new CProps
        {
            relA = Mathf.Clamp01(a.relA),
            relB = Mathf.Clamp01(a.relB),
            relC = Mathf.Clamp01(a.relC),
            agr = Mathf.Clamp01(a.agr),
            crown = Mathf.Clamp01(a.crown),
            armor = Mathf.Clamp01(a.armor),
            econ = Mathf.Clamp01(a.econ),
        };
        return ret;
    }

    // ������-�� ����� ���������� ������� � ��������, �����������
    public CProps GenPropNoiseWithLimits(CProps inProps, CProps inNoise, CProps inNorms)
    {
        CProps ret = new CProps();

        float relNoise = inNoise.relA * noiseAmpPropsCom.relA * noiseK;
        ret.relA = inProps.relA + relNoise * RandRangeWithLimits(inProps.relA, inNorms.relA, noiseLimRel); // �� ������� �� �
        ret.relB = inProps.relB + relNoise * RandRangeWithLimits(inProps.relB, inNorms.relA, noiseLimRel); // �� ������� �� �
        ret.relC = inProps.relC + relNoise * RandRangeWithLimits(inProps.relC, inNorms.relA, noiseLimRel); // �� ������� �� �


        float agrNoise = inNoise.agr * noiseAmpPropsCom.agr * noiseK;
        ret.agr = inProps.agr + agrNoise * RandRangeWithLimits(inProps.agr, inNorms.agr, noiseLimAgr);


        float crownNoise = inNoise.crown * noiseAmpPropsCom.crown * noiseK;
        ret.crown = inProps.crown + crownNoise * RandRangeWithLimits(inProps.crown, inNorms.crown, noiseLimCrown);


        float armorNoise = inNoise.armor * noiseAmpPropsCom.armor * noiseK;
        ret.armor = inProps.armor + armorNoise * RandRangeWithLimits(inProps.armor, inNorms.armor, noiseLimArmor);


        float econNoise = inNoise.econ * noiseAmpPropsCom.econ * noiseK;
        ret.econ = inProps.econ + econNoise * RandRangeWithLimits(inProps.econ, inNorms.econ, noiseLimEcon);

        return ret;
    }
    float RandRangeWithLimits(float inCur, float inNorm, float inLimitK)
    {
        float hiR = 1;
        float lowR = -1;
        /*
        if (inCur > inNorm + inLimitK) hiR = 0;
        if (inCur < inNorm - inLimitK) lowR = 0;*/
        if (inCur > inNorm + inLimitK || inCur < inNorm - inLimitK)
        {
            return 0;
        }

        // ����� ������ ���� �������� ������ �������� ��������
        return Random.Range(lowR, hiR);
    }
    public CProps GenPropNoise(CProps inProps, CProps inNoise)
    {
        CProps ret = new CProps();

        float relNoise = inNoise.relA * noiseAmpPropsCom.relA * noiseK;
        ret.relA = inProps.relA + Random.Range(-relNoise, relNoise);
        ret.relB = inProps.relB + Random.Range(-relNoise, relNoise);
        ret.relC = inProps.relC + Random.Range(-relNoise, relNoise);

        float agrNoise = inNoise.agr * noiseAmpPropsCom.agr * noiseK;
        ret.agr = inProps.agr + Random.Range(-agrNoise, agrNoise);

        float crownNoise = inNoise.crown * noiseAmpPropsCom.crown * noiseK;
        ret.crown = inProps.crown + Random.Range(-crownNoise, crownNoise);

        float armorNoise = inNoise.armor * noiseAmpPropsCom.armor * noiseK;
        ret.armor = inProps.armor + Random.Range(-armorNoise, armorNoise);

        float econNoise = inNoise.econ * noiseAmpPropsCom.econ * noiseK;
        ret.econ = inProps.econ + Random.Range(-econNoise, econNoise);

        return ret;
    }
    public CProps DempPropOldLerp(CProps a, CProps d)
    {
        CProps ret = new CProps();

        float relDemp = d.relA * dempKPropsCom.relA;
        ret.relA = Mathf.Lerp(a.relA, normPropsCom.relA, relDemp);
        ret.relB = Mathf.Lerp(a.relB, normPropsCom.relB, relDemp);
        ret.relC = Mathf.Lerp(a.relC, normPropsCom.relC, relDemp);

        float agrDemp = d.agr * dempKPropsCom.agr;
        ret.agr = Mathf.Lerp(a.agr, normPropsCom.agr, agrDemp);

        float crownDemp = d.crown * dempKPropsCom.crown;
        ret.crown = Mathf.Lerp(a.crown, normPropsCom.crown, crownDemp);

        float armorDemp = d.armor * dempKPropsCom.armor;
        ret.armor = Mathf.Lerp(a.armor, normPropsCom.armor, armorDemp);

        float econDemp = d.econ * dempKPropsCom.econ;
        ret.econ = Mathf.Lerp(a.econ, normPropsCom.econ, econDemp);

        return ret;
    }

    public CProps StepDempProp(CProps a, CProps norms, CProps d)
    {
        CProps ret = new CProps();

        float relDemp = d.relA * dempKPropsCom.relA * dempK;
        ret.relA = StepDemp(a.relA, norms.relA, relDemp);
        ret.relB = StepDemp(a.relB, norms.relB, relDemp);
        ret.relC = StepDemp(a.relC, norms.relC, relDemp);

        float agrDemp = d.agr * dempKPropsCom.agr * dempK;
        ret.agr = StepDemp(a.agr, norms.agr, agrDemp);

        float crownDemp = d.crown * dempKPropsCom.crown * dempK;
        ret.crown = StepDemp(a.crown, norms.crown, crownDemp);

        float armorDemp = d.armor * dempKPropsCom.armor * dempK;
        ret.armor = StepDemp(a.armor, norms.armor, armorDemp);

        float econDemp = d.econ * dempKPropsCom.econ * dempK;
        ret.econ = StepDemp(a.econ, norms.econ, econDemp);

        return ret;
    }
    float StepDemp(float p, float n, float step)
    {
        // if (Mathf.Abs(p - n) < step) return n;
        if (Mathf.Abs(p - n) < step) return p;

        if (p > n) return p - step;
        return p + step;
    }


    public CProps NormalizeRels(CProps a)
    {
        float sumI = a.relA + a.relB + a.relC;
        float nRelA = a.relA / sumI;
        float nRelB = a.relB / sumI;
        float nRelC = a.relC / sumI;
        a.relA = nRelA;
        a.relB = nRelB;
        a.relC = nRelC;

        return a;
    }

    public void InitCalc(SceneVals scn)
    {
        normPropsCom = Glob.Countries[0].prop;
        dempKPropsCom = Glob.Countries[0].dempProp;
        noiseAmpPropsCom = Glob.Countries[0].noiseProp;
    }

    public void IntegrateProps(int countID)
    {
        Glob.Countries[countID].prop =
            LerpProps(Glob.Countries[countID].prop,
            Glob.Countries[countID].preProp,
            IntegrateK);
    }
    public void CalcRelateMx()
    {
        int len = Glob.Countries.Length - 4; // ��������� 4 ��� ������, �� �������� ������ ��� ���

        Glob.RelateMx = new CRelation[len, len];
        for (int i = 0; i < len; i++)
        {
            for (int j = i; j < len; j++)
            {
                Glob.RelateMx[i, j] = new CRelation
                {
                    Tens = CalcTens(Glob.Countries[i], Glob.Countries[j])
                };


                Glob.RelateMx[j, i] = new CRelation
                {
                    Tens = Glob.RelateMx[i, j].Tens
                };

                // print("i=" + i + " j=" + j + " tens=" + Glob.RelateMx[i, j].Tens);
            }
        }

    }
    public bool CanPutArmor(Agent ag, int cnId)
    {
        return ag.type == "armor" && Glob.Countries[cnId].prop.armor < ArmorTreshForAccept ;
        
    }
}
