using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TensElPan : MonoBehaviour
{
    public Image cntAImg;
    public Image cntBImg;
    public TMP_Text tensTxt;
    public CanvasGroup canvasGrp;
    public AnimationCurve fadeCrv;

    int myCntA;
    int myCntB;

    public bool isActive;
    public float showTime=2;
    float time0;
    float time1;

    private void Start()
    {
        Glob.DoTick += UpView;
    }

    public void StartView(int cntA,int cntB)
    {
        time0 = Time.time;
        time1 = Time.time + showTime;
        gameObject.SetActive(true);
        isActive = true;
        myCntA = cntA;
        myCntB = cntB;
        cntAImg.color = Glob.Countries[myCntA].cellCol;
        cntBImg.color = Glob.Countries[myCntB].cellCol;
        UpView();
    }
    void UpView()
    {
        if (!isActiveAndEnabled) return;

        float tens;
        // float tens = Glob.Calc.CalcTens(Glob.Countries[myCntA], Glob.Countries[myCntB]);
        if (Glob.Countries[myCntB].isActive) 
        { 
            tens = Glob.RelateMx[myCntA, myCntB].Tens;
        } else
        {
            tens = 0;
        }
        tensTxt.text = (tens * 100).ToString("n0") + "%";
        tensTxt.color = Glob.MField.frontCols.Evaluate(tens);
    }
    public void StopView()
    {
        isActive = false;
        gameObject.SetActive(false);
    }
    private void Update()
    {
        if (isActive)
        {
            if (Time.time >= time1)
            {
                StopView();
            } else
            {
                float k = Mathf.InverseLerp(time0, time1, Time.time);
                canvasGrp.alpha = fadeCrv.Evaluate(k);
            }

        }
    }


}
