using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class GoalPan : MonoBehaviour
{
    public TMP_Text goalTxt;
    public CanvasGroup canvasGrp;
    public AnimationCurve fadeCrv;
    public bool isActive;
    public float showTime = 2;
    float time0;
    float time1;

    private void Start()
    {
        canvasGrp.alpha = 0;
        StopView();
    }
    public void OnBut()
    {
        if (isActive && (Mathf.InverseLerp(time0, time1, Time.time) < .8f))
        {
            FadeOutView();
        } else
        {
            StartView();
        }
    }
    public void StartView()
    {
        time0 = Time.time;
        time1 = Time.time + showTime;
        gameObject.SetActive(true);
        isActive = true;

        goalTxt.text = Glob.sceneVals[Glob.sceneNum].goal;
    }
    public void FadeOutView()
    {
        time0 = Time.time - showTime * .8f;
        time1 = Time.time + showTime * .2f;
        Update();
    }
    public void StopView()
    {
        isActive = false;
        gameObject.SetActive(false);
    }
    private void Update()
    {
        if (isActive)
        {
            if (Time.time >= time1)
            {
                StopView();
            }
            else
            {
                float k = Mathf.InverseLerp(time0, time1, Time.time);
                canvasGrp.alpha = fadeCrv.Evaluate(k);
            }

        }
    }


}
