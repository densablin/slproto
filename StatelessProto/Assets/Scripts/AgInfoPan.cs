using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class AgInfoPan : MonoBehaviour
{
    
    public Image AImg;
    public TMP_Text AnnoTxt;
    public TMP_Text PriceTxt;
    public Image InfPicA;
    public Image InfPicB;
    public Image InfBorder;
    public CanvasGroup cGr;
    public AnimationCurve fadeCrv;

    public float showTime = 3f;
    public float time0;
    public float time1;
    public bool isShow;
    public bool testShow;

    void Start()
    {
        HidePan();
    }

    void Update()
    {
        if (testShow)
        {
            testShow = false;
            Agent ag = new Agent();
            ShowPan(ag);
        }

        if (isShow)
        {
            if (Time.time >= time1)
            {
                HidePan();
            } else
            {
                float k = Mathf.InverseLerp(time0, time1, Time.time);
                cGr.alpha = fadeCrv.Evaluate(k);
            }
        }
        
    }
    public void ShowPan(Agent ag)
    {
        isShow = true;
        time0 = Time.time;
        time1 = Time.time + showTime;
        AnnoTxt.text = ag.annotation;
        string prStr = "";
        if (ag.price < 0) // �� ��� �� ��������, ���������� � ������� ������������� ����
        {
            prStr = "+";
        }
        else if (ag.price > 0)
        {
            prStr = "�";
        }
        PriceTxt.text = prStr + Math.Abs(ag.price);
        InfPicA.sprite = Glob.glyphLib[ag.iconPicA];
        InfPicB.sprite = Glob.glyphLib[ag.iconPicB];
        AImg.sprite = Glob.glyphLib[ag.infoPic];
        InfBorder.color = ag.baseCol;
    }
    public void HidePan()
    {
        isShow = false;
        cGr.alpha = 0;
    }
}

