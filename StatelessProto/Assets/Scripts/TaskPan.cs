using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TaskPan : MonoBehaviour
{
    [SerializeField] TMP_Text mainTxt;
    [SerializeField] TMP_Text titleTxt;
    [SerializeField] CanvasGroup canvGroup;
    [SerializeField] RectTransform[] ButGlyphs;
    //[SerializeField] RectTransform ButGlyph0;
    //[SerializeField] RectTransform ButGlyph1;
    [SerializeField] float fadeTime;
    [SerializeField] AnimationCurve fInCur;
    [SerializeField] AnimationCurve fOutCur;

    float time0;
    float time1;
    panState myState = panState.hide;

    public void Awake()
    {
        Glob.TaskPan = this;
    }

    void Start()
    {
        gameObject.SetActive(false);
        myState = panState.hide;
        canvGroup.alpha = 0;
    }
    public void StartView()
    {
        gameObject.SetActive(true);

        SetContent();
        if (myState == panState.hide || myState == panState.fadeOut)
        {
            time0 = Time.time;
            time1 = Time.time + fadeTime;
            myState = panState.fadeIn;

            Glob.GMan.lastTickSpeed = Glob.GMan.tickSpeed;
            Glob.TickButsPan.ButClick((int)GameMan.ts.stop);
        }
    }
    public void StopView()
    {
        Glob.TickButsPan.ButClick((int)Glob.GMan.lastTickSpeed);

        if (myState == panState.show || myState == panState.fadeIn)
        {
            time0 = Time.time;
            time1 = Time.time + fadeTime;
            myState = panState.fadeOut;
        }
    }
    public void OnBut()
    {
        if (myState == panState.hide || myState == panState.fadeOut)
        {
            StartView();
        }
        else
        {
            StopView();
        }
    }

    public void GoBut()
    {
        StopView();
        scConditionsData scCond = Glob.GMan.FindInConditions("next");

        if (scCond != null)
        {
            Glob.GMan.SceneButtonClick((int)scCond.val);
        }
    }

    void Update()
    {
        if (Time.time >= time1) SetNexState();

        canvGroup.alpha = myState switch
        {
            panState.hide => 0,
            panState.fadeIn => fInCur.Evaluate(Mathf.InverseLerp(time0, time1, Time.time)),
            panState.show => 1,
            panState.fadeOut => fOutCur.Evaluate(Mathf.InverseLerp(time0, time1, Time.time)),
            _ => 1,
        };

    }
    void SetNexState()
    {
        switch (myState)
        {
            case panState.fadeIn:
                myState = panState.show;
                break;
            case panState.fadeOut:
                myState = panState.hide;
                gameObject.SetActive(false);
                break;
        }
    }
    public void SetContent()
    {
        titleTxt.text = Glob.sceneVals[Glob.sceneNum].title;
        mainTxt.text = Glob.sceneVals[Glob.sceneNum].annot
            + "<br><br><color=#FFE799>" + Glob.sceneVals[Glob.sceneNum].goal
            + "</color>"
            ;

        for (int i = 0; i < ButGlyphs.Length; i++)
        {
            ButGlyphs[i].gameObject.SetActive(false);
            if (i == Glob.sceneVals[Glob.sceneNum].taskPanGlyphNum)
                ButGlyphs[i].gameObject.SetActive(true);
        }
    }
}
