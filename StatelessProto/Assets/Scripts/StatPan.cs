using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatPan : MonoBehaviour
{
    // public Image CapImageBg;
    public RectTransform RelAPan;
    public RectTransform RelAInd;
    public RectTransform RelBPan;
    public RectTransform RelBInd;
    public RectTransform RelCPan;
    public RectTransform RelCInd;
    public RectTransform crownPan;
    public RectTransform crownInd;
    public RectTransform agrPan;
    public RectTransform agrInd;
    public RectTransform armorPan;
    public RectTransform armorInd;
    public RectTransform econPan;
    public RectTransform econInd;

    public RectTransform resizerPan;
    public RectTransform uhoPan;

    public bool relAOn = true;
    public bool relBOn = true;
    public bool relCOn = true;
    public bool crownOn = false;
    public bool agrOn = false;
    public bool armorOn = false;
    public bool econOn = false;
    public bool testInitPan;

    public Image CapImage;
    public float TestRelA = .5f;

    void Start()
    {
        Glob.StatPan = this;
        Glob.DoTick += ShowVals;
        UpdatePans();
    }

    void Update()
    {
        if (testInitPan)
        {
            testInitPan = false;
            UpdatePans();
        }
    }
    public void UpdatePans()
    {
        RelAPan.gameObject.SetActive(relAOn);
        RelBPan.gameObject.SetActive(relBOn);
        RelCPan.gameObject.SetActive(relCOn);
        crownPan.gameObject.SetActive(crownOn);
        agrPan.gameObject.SetActive(agrOn);
        armorPan.gameObject.SetActive(armorOn);
        econPan.gameObject.SetActive(econOn);

        LayoutRebuilder.ForceRebuildLayoutImmediate(resizerPan);
        Canvas.ForceUpdateCanvases();

        uhoPan.sizeDelta = new Vector2(resizerPan.sizeDelta.x, uhoPan.sizeDelta.y);

    }
    public void InitStatPans(SceneVals sc)
    {
        ReadStatPansFromData(sc.statPanEls);
    }
    public void ReadStatPansFromData(string[] inStrs)
    {
        relAOn = false;
        relBOn = false;
        relCOn = false;
        crownOn = false;
        agrOn = false;
        armorOn = false;
        econOn = false;
        foreach (var str in inStrs)
        {
            switch (str)
            {
                case "rel":
                    {
                        relAOn = true;
                        relBOn = true;
                        relCOn = true;
                    };
                    break;
                case "crown":
                    crownOn = true; break;
                case "agr":
                    agrOn = true; break;
                case "armor":
                    armorOn = true; break;
                case "econ":
                    econOn = true; break;
            }
        }
        UpdatePans();
    }
    private void LateUpdate()
    {
        // uhoPan.sizeDelta = new Vector2(resizerPan.sizeDelta.x, uhoPan.sizeDelta.y);
    }
    void ShowVals()
    {
        Color cCol = Glob.Countries[Glob.ShowStatCountryID].cellCol;
        // CapImageBg.color = cCol;
        CapImage.color = cCol; // Color.Lerp(Color.white,cCol,.5f);

        RelAInd.anchorMin = new Vector2(0f, 0f);
        RelAInd.anchorMax = new Vector2(1f, Glob.Countries[Glob.ShowStatCountryID].prop.relA);
        RelBInd.anchorMin = new Vector2(0f, 0f);
        RelBInd.anchorMax = new Vector2(1f, Glob.Countries[Glob.ShowStatCountryID].prop.relB);
        RelCInd.anchorMin = new Vector2(0f, 0f);
        RelCInd.anchorMax = new Vector2(1f, Glob.Countries[Glob.ShowStatCountryID].prop.relC);
        agrInd.anchorMin = new Vector2(0f, 0f);
        agrInd.anchorMax = new Vector2(1f, Glob.Countries[Glob.ShowStatCountryID].prop.agr);
        crownInd.anchorMin = new Vector2(0f, 0f);
        crownInd.anchorMax = new Vector2(1f, Glob.Countries[Glob.ShowStatCountryID].prop.crown);
        armorInd.anchorMin = new Vector2(0f, 0f);
        armorInd.anchorMax = new Vector2(1f, Glob.Countries[Glob.ShowStatCountryID].prop.armor);
        econInd.anchorMin = new Vector2(0f, 0f);
        econInd.anchorMax = new Vector2(1f, Glob.Countries[Glob.ShowStatCountryID].prop.econ);

    }
    public void ShowAll()
    {
        if (relAOn && relBOn && relCOn && crownOn && agrOn && armorOn && econOn)
        {
            InitStatPans(Glob.sceneVals[Glob.sceneNum]);
        }
        else
        {
            relAOn = true;
            relBOn = true;
            relCOn = true;
            crownOn = true;
            agrOn = true;
            armorOn = true;
            econOn = true;
            UpdatePans();
        }
    }
}

// LayoutRebuilder.ForceRebuildLayoutImmediate(resizerPan);
// Canvas.ForceUpdateCanvases();
