﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataGen : MonoBehaviour
{

    public glyphEl[] glyphLib;
    public Dictionary<string, Agent> AgentLib;
    public Dictionary<string, Color> aColsLib;

    private void Awake()
    {
        Glob.DGen = this;
    }
    public void InitReadLibs()
    {
        ReadGlyphLib();
        ReadAgColsLib();
        ReadAgentLib();
        ReadSceneVals();
    }
    public void ReadGlyphLib()
    {
        Glob.glyphLib = new Dictionary<int, Sprite>();
        foreach (var gl in glyphLib)
        {
            Glob.glyphLib.Add(gl.id, gl.spr);
        }
    }
    public void ReadAgColsLib()
    {
        aColsLib = new Dictionary<string, Color>()
        {
            { "relA", new Color(.6f,0,.8f) },
            { "relB", new Color(0,.5f,.5f) },
            { "relC", new Color(.85f,.6f,.05f) }, 
            { "crown", new Color(.67f,0f,.37f)  },
            { "agr", new Color(1f,.2f,0f)  },
            { "armor", new Color(.43f,.64f,0f)  }, 
        };
    }
    public void ReadAgentLib()
    {
        AgentLib = new Dictionary<string, Agent>();

        Agent ag = new Agent
        {
            agID = "RelA+1",
            iconPicA = 0,
            iconPicB = 10,
            infoPic = 21,
            baseCol = aColsLib["relA"],
            type = "relA",
            annotation = "Поддержать тур проповедника",
            price = 100,
            effect = new effectData[]
                {
                    new effectData{ var = "relA", val = 1f },
                    new effectData{ var = "relB", val = 0f },
                    new effectData{ var = "relC", val = 0f }
                }

        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "RelA+3",
            iconPicA = 0,
            iconPicB = 11,
            infoPic = 20,
            baseCol = aColsLib["relA"],
            type = "relA",
            annotation = "Объявить собор религиозной общины",
            price = 500,
            effect = new effectData[]
                {
                    new effectData{ var = "relA", val = 3f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "RelA+5",
            iconPicA = 0,
            iconPicB = 12,
            infoPic = 20,
            baseCol = aColsLib["relA"],
            type = "relA",
            annotation = "Финансировать вещание религиозной радиостанции",
            price = 1000,
            effect = new effectData[]
                {
                    new effectData{ var = "relA", val = 5f },
                }

        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "RelB+1",
            iconPicA = 1,
            iconPicB = 10,
            infoPic = 20,
            baseCol = aColsLib["relB"],
            type = "relB",
            annotation = "Поддержать выступление агитатора",
            price = 100,
            effect = new effectData[]
                {
                    new effectData{ var = "relB", val = 1f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "RelB+3",
            iconPicA = 1,
            iconPicB = 11,
            infoPic = 20,
            baseCol = aColsLib["relB"],
            type = "relB",
            annotation = "Начать прокат киноблокбастера",
            price = 500,
            effect = new effectData[]
                {
                    new effectData{ var = "relB", val = 3f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "RelB+5",
            iconPicA = 1,
            iconPicB = 12,
            infoPic = 20,
            baseCol = aColsLib["relB"],
            type = "relB",
            annotation = "Выпустить в эфир публицистическое телешоу",
            price = 1000,
            effect = new effectData[]
                {
                    new effectData{ var = "relB", val = 5f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "RelC+1",
            iconPicA = 2,
            iconPicB = 10,
            infoPic = 20,
            baseCol = aColsLib["relC"],
            type = "relC",
            annotation = "Рекламировать книги писателя-фанатика",
            price = 50,
            effect = new effectData[]
                {
                    new effectData{ var = "relC", val = 1f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "RelC+3",
            iconPicA = 2,
            iconPicB = 11,
            infoPic = 20,
            baseCol = aColsLib["relC"],
            type = "relC",
            annotation = "Издать тираж тематической газеты",
            price = 100,
            effect = new effectData[]
                {
                    new effectData{ var = "relC", val = 3f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "RelC+5",
            iconPicA = 2,
            iconPicB = 12,
            infoPic = 20,
            baseCol = aColsLib["relC"],
            type = "relC",
            annotation = "Провести конгресс политической организации",
            price = 500,
            effect = new effectData[]
                {
                    new effectData{ var = "relC", val = 5f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "Agr+5",
            iconPicA = 3,
            iconPicB = 12,
            infoPic = 20,
            baseCol = aColsLib["agr"],
            type = "agr",
            annotation = "Кругом враги!",
            price = 500,
            effect = new effectData[]
                {
                    new effectData{ var = "agr", val = 5f },
                }
        };

        AgentLib.Add(ag.agID, ag);
        ag = new Agent
        {
            agID = "Agr-5",
            iconPicA = 3,
            iconPicB = 15,
            infoPic = 21,
            baseCol = aColsLib["agr"],
            type = "agr",
            annotation = "Мы за мирные переговоры!",
            price = 100,
            effect = new effectData[]
                {
                    new effectData{ var = "agr", val = -5f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "Crown+5",
            iconPicA = 4,
            iconPicB = 12,
            infoPic = 21,
            baseCol = aColsLib["crown"],
            type = "crown",
            annotation = "Запустить кампанию: «Единый лидер – крепкая страна!»",
            price = 500,
            effect = new effectData[]
                {
                    new effectData{ var = "crown", val = +5f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "Crown-5",
            iconPicA = 4,
            iconPicB = 15,
            infoPic = 20,
            baseCol = aColsLib["crown"],
            type = "crown",
            annotation = "Устроить народные возмущения: «Даешь альтернативные выборы!»",
            price = 500,
            effect = new effectData[]
                {
                    new effectData{ var = "crown", val = -5f },
                }
        };
        AgentLib.Add(ag.agID, ag);

        ag = new Agent
        {
            agID = "Armor+5",
            iconPicA = 5,
            iconPicB = 12,
            infoPic = 21,
            baseCol = aColsLib["armor"],
            type = "armor",
            annotation = "Осуществить поставку вооружения",
            price = -2500, // т.е. "-" -- зачит добавляем в кассу
            effect = new effectData[]
                {
                    new effectData{ var = "armor", val = +5f },
                }
        };
        AgentLib.Add(ag.agID, ag);


        // обязательно конвертируем эффекты в реальные пропсы
        foreach (var a in AgentLib)
        {
            a.Value.prop = Calc.ConvertEffectToProps(a.Value.effect);
            // print("AgentLib=" + a.Value.agID + " Ann=" + a.Value.annotation );

        }

    }
    public void ReadSceneVals()
    {
        Glob.sceneVals = new List<SceneVals>();

        SceneVals sv = new SceneVals
        {
            title = "Интро",
            annot = "Где Маркус назначит аудиенцию в следующий раз – заранее не знает никто. " +
            "Скорее всего, это будет один из десятков островов, принадлежащих ему. " +
            "Вожди, короли и президенты приезжают к Маркусу на поклон. Всё ради одного – ради оружия, "+
            "которое он может им продать. " +
            "<line-height=175%><br><line-height=100%>" +
            "Впрочем, старого Маркуса уже не интересуют деньги: " +
            "у него есть всё, что можно за них купить, и почти всё, чего за деньги купить нельзя. "+
            "Его интерес – найти того, с кем можно поделиться своим знанием. " +
            "<line-height=175%><br><line-height=100%>" +
            "Если ты хочешь зарабатывать на войне – ты должен знать о войне всё. Послушай же старого Маркуса...",
            goal = "",
            cntVals = new scCntData[]
                    { new scCntData
                            { cntID = 0,
                              capPlace = new Vector3(0, 0, 0),
                              areaMin = 10,
                              areaNorm = 10,
                              areaMax = 10,
                              initEffects = new effectData[] // сет пропсов по умолчанию для всех стран сцены
                                                             // их считаем за норму, к ним демпингуем
                              {
                                new effectData{ var = "relA", val = 0.33f },
                                new effectData{ var = "relB", val = 0.33f },
                                new effectData{ var = "relC", val = 0.33f },
                                new effectData{ var = "crown", val = 1f },
                                new effectData{ var = "agr", val = 0.5f },
                                new effectData{ var = "armor", val = 1f },
                                new effectData{ var = "econ", val = 1f },
                              },
                              
                              noiseEffects = new effectData[] // коэффициент шума от 0 до 1 к стандартному
                                                              // для всех на сцене
                              {
                                new effectData{ var = "relA", val = 1f },
                                new effectData{ var = "relB", val = 1f },
                                new effectData{ var = "relC", val = 1f },
                                new effectData{ var = "crown", val = .2f },
                                new effectData{ var = "agr", val = 1f },
                                new effectData{ var = "armor", val = .2f },
                                new effectData{ var = "econ", val = 1f },
                              },
                              dempEffects = new effectData[] // коэффициент демпинга от 0 до 1 к стандартному
                                                             // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .3f },
                                new effectData{ var = "relB", val = .3f },
                                new effectData{ var = "relC", val = .3f },
                                new effectData{ var = "crown", val = 1f },
                                new effectData{ var = "agr", val = 1f },
                                new effectData{ var = "armor", val = 1f },
                                new effectData{ var = "econ", val = 1f },
                              },

                            } ,
                     new scCntData
                            { cntID = 1,
                              capPlace = new Vector3(15, 8, -23),
                              areaMin = 20,
                              areaNorm = 50,
                              areaMax = 50,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "crown", val = 1f },
                              },
                            } ,
                     },
            conds = new scConditionsData[]
                    { new scConditionsData
                        {
                            par = "next",
                            val = 1,
                        }
                    },
            statPanEls = new string[]
                    {
                      //"rel",
                      "crown",
                      //"agr",
                      //"econ",
                      "armor",
                    },
            toolPanEls = new string[]
                    {
                      //"RelA+1",
                      //"RelA+3",
                      //"RelA+5",
                      //"RelB+1",
                      //"RelB+3",
                      //"RelB+5",
                      //"RelC+1",
                      //"RelC+3",
                      //"RelC+5",
                    },
            taskPanGlyphNum = 0,
            
        };

        Glob.sceneVals.Add(sv);

        sv = new SceneVals
        {
            title = "Туториал. Глава 1. Идеология",
            annot = "Старый Маркус говорит: <br>– Война начинается с идеи в голове. " +
                    "Нет, не с идеи войны. А с той самой Идеи в которую ты веришь, которую считаешь верной. " +
                    "И чем более ты своей Идеей захвачен, тем искренне ты захочешь убить того, " +
                    "кто её отвергает, кто верит совсем не в ту, что тебе дорога." +
                    "<line-height=175%><br><line-height=100%>" +
                    "Навяжи двум странам две разные идеологии – и они начнут войну между собой.",
            goal = "\u2666 Максимально подними показатель любой из идеологий " +
                    "<size=120%><b><color=#C000C0>[α]</color></b></size>, " +
                    "<size=120%><b><color=#00C0C0>[β]</color></b></size> или " +
                    "<size=120%><b><color=#FFc080>[c]</color></b></size> " +
                    "в каждой стране, и тогда на границе стран " +
                    "с противоположными взглядами будет спровоцирован конфликт.",
            // activeCntNum = 3,
            cntVals = new scCntData[] 
                    { new scCntData 
                            { cntID = 0,
                              capPlace = new Vector3(0, 0, 0),
                              areaMin = 20,
                              areaNorm = 200,
                              areaMax = 40,
                              initEffects = new effectData[] // сет пропсов по умолчанию для всех стран сцены
                                                             // их считаем за норму, к ним демпингуем
                              {
                                new effectData{ var = "relA", val = 0.33f },
                                new effectData{ var = "relB", val = 0.33f },
                                new effectData{ var = "relC", val = 0.33f },
                                new effectData{ var = "crown", val = 0.0f },
                                new effectData{ var = "agr", val = .50f },
                                new effectData{ var = "armor", val = .5f },
                                new effectData{ var = "econ", val = 1f },
                              },
                              noiseEffects = new effectData[] // коэффициент шума от 0 до 1 к стандартному
                                                              // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .2f },
                                new effectData{ var = "relB", val = .2f },
                                new effectData{ var = "relC", val = .2f },
                                new effectData{ var = "crown", val = 0f },
                                new effectData{ var = "agr", val = .1f },
                                new effectData{ var = "armor", val = 0.1f },
                                new effectData{ var = "econ", val = 0f },
                              },
                              dempEffects = new effectData[] // коэффициент демпинга от 0 до 1 к стандартному
                                                             // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .3f },
                                new effectData{ var = "relB", val = .3f },
                                new effectData{ var = "relC", val = .3f },
                                new effectData{ var = "crown", val = 0f },
                                new effectData{ var = "agr", val = .5f },
                                new effectData{ var = "armor", val = 2f },
                                new effectData{ var = "econ", val = 1f },
                              },
                              

                            } ,
                     new scCntData
                            { cntID = 1,
                              capPlace = new Vector3(19, 1, -20),
                              areaMin = 20,
                              areaNorm = 60,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relA", val =.5f },
                              },
                            } ,
                      new scCntData
                            { cntID = 2,
                              capPlace = new Vector3(19, 10, -29),
                              areaMin = 20,
                              areaNorm = 120,
                              areaMax = 40,
                              initEffects = new effectData[]    
                              {
                                  new effectData { var = "relC", val = .5f },
                              },

                            },
                      new scCntData
                            { cntID = 3,
                              capPlace = new Vector3(10, 10, -20),
                              areaMin = 20,
                              areaNorm = 120,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relB", val = .5f },
                              },

                            }
                     },
            conds = new scConditionsData[] 
                    { new scConditionsData
                        { 
                            par = "xp",
                            val = 1000,
                        }
                    },
            statPanEls = new string[]
                    {
                      "rel",
                      //"crown",
                      //"agr",
                      //"armor",
                      //"econ",
                    },
            toolPanEls = new string[]
                    { 
                      "RelA+1",
                      "RelA+3",
                      "RelA+5",
                      "RelB+1",
                      "RelB+3",
                      "RelB+5",
                      "RelC+1",
                      "RelC+3",
                      "RelC+5",
                    },
            taskPanGlyphNum = 1,
        };

        Glob.sceneVals.Add(sv);

        sv = new SceneVals
        {
            title = "Туториал. Глава 2. Автократия",
            annot = "Старый Маркус говорит: <br>– Война начинается с идеологии. " +
                    "Но идея может сидеть в человеческой голове абы как: " +
                    "подвернулся подходящий случай – и ты увлекся другой. " +
                    "В этом радость свободы, лицемерно утверждают некоторые малодушные. " +
                    "<line-height=175%><br><line-height=100%>" +
                    "Но в стране, где ценят власть, где она крепка, едина и долговечна – в головах людей тоже всё крепко. " +
                    "А если кто соблазнится искушением думать иначе – государственный лидер укажет ему, во что мы все верим." +
                    "<line-height=175%><br><line-height=100%>" +
                    "Меньше демократии – меньше разногласий.",
            goal = "\u2666 Максимально подними показатель автократии " +
                    "<size=120%><b><color=#AD0360>[\u25a0]</color></b></size>" + 
                    "в стране, и тогда война между идеологическими врагами будет длится гораздо дольше. " +
                    "Кстати, ты же понимаешь, потеряв столицу – потеряешь единую власть." ,
            // activeCntNum = 3,
            cntVals = new scCntData[]
                    { new scCntData
                            { cntID = 0,
                              capPlace = new Vector3(0, 0, 0),
                              areaMin = 20,
                              areaNorm = 200,
                              areaMax = 40,
                              initEffects = new effectData[] // сет пропсов по умолчанию для всех стран сцены
                                                             // их считаем за норму, к ним демпингуем
                              {
                                new effectData{ var = "relA", val = 0.33f },
                                new effectData{ var = "relB", val = 0.33f },
                                new effectData{ var = "relC", val = 0.33f },
                                new effectData{ var = "crown", val = 0.1f },
                                new effectData{ var = "agr", val = 0.5f },
                                new effectData{ var = "armor", val = 0.5f },
                                new effectData{ var = "econ", val = 1f },
                              },
                              noiseEffects = new effectData[] // коэффициент шума от 0 до 1 к стандартному
                                                              // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .5f },
                                new effectData{ var = "relB", val = .5f },
                                new effectData{ var = "relC", val = .5f },
                                new effectData{ var = "crown", val = .5f },
                                new effectData{ var = "agr", val = .1f },
                                new effectData{ var = "armor", val = 0.1f },
                                new effectData{ var = "econ", val = 0f },
                              },
                              dempEffects = new effectData[] // коэффициент демпинга от 0 до 1 к стандартному
                                                             // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .5f },
                                new effectData{ var = "relB", val = .5f },
                                new effectData{ var = "relC", val = .5f },
                                new effectData{ var = "crown", val = 1f },
                                new effectData{ var = "agr", val = .5f },
                                new effectData{ var = "armor", val = 1f },
                                new effectData{ var = "econ", val = 1f },
                              },
                            } ,
                     new scCntData
                            { cntID = 1,
                              capPlace = new Vector3(19, 1, -20),
                              areaMin = 20,
                              areaNorm = 60,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relA", val =.5f },
                              },
                            } ,
                      new scCntData
                            { cntID = 2,
                              capPlace = new Vector3(19, 10, -29),
                              areaMin = 20,
                              areaNorm = 120,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relC", val = .5f },
                              },

                            },
                      new scCntData
                            { cntID = 3,
                              capPlace = new Vector3(10, 10, -20),
                              areaMin = 20,
                              areaNorm = 120,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relB", val = .5f },
                              },

                            }
                     },
            conds = new scConditionsData[]
                    { new scConditionsData
                        {
                            par = "xp",
                            val = 1000,
                        }
                    },
            statPanEls = new string[]
                    {
                      "rel",
                      "crown",
                      //"agr",
                      //"armor",
                      //"econ",
                    },
            toolPanEls = new string[]
                    {
                      //"RelA+1",
                      "RelA+3",
                      "RelA+5",
                      //"RelB+1",
                      "RelB+3",
                      "RelB+5",
                      //"RelC+1",
                      "RelC+3",
                      "RelC+5",
                      "Crown+5",
                      "Crown-5"
                      //"Agr+5",
                      //"Agr-5",
                    },
            taskPanGlyphNum = 1,
        };

        Glob.sceneVals.Add(sv);

        sv = new SceneVals
        {
            title = "Туториал. Глава 3. Агрессия",
            annot = "Старый Маркус говорит: <br>– Война за то, чья идеология главнее – любимая забава монархов. " +
                    "Но в топке Машины войны должно пылать топливо: ненависть, гордыня, страх." +
                    "<line-height=175%><br><line-height=100%>" +
                    "Скажи матерям, что идеологический враг покушается на души их детей. " +
                    "Объяви мирным гражданам, что войска соседской страны мечтают их поработить. " +
                    "Шепни главе государства, что окрестные царьки сговариваются о его смерти. " +
                    "И ты увидишь, как приходят в движение поршни войны." +
                    "<line-height=175%><br><line-height=100%>" +
                    "Всего лишь пара нехитрых лозунгов, пара подложных фактов – и страна-агрессор готова крушить",
            goal = "\u2666 Максимально подними показатель агрессии "+
                    "<size=120%><b><color=#FF2700>[\u25a0]</color></b></size>" +
                    "в стране, и увидишь на что способна такая держава. " +
                    "Опусти этот показатель – и страна примет нейтралитет.",
            // activeCntNum = 3,
            cntVals = new scCntData[]
                    { new scCntData
                            { cntID = 0,
                              capPlace = new Vector3(0, 0, 0),
                              areaMin = 20,
                              areaNorm = 200,
                              areaMax = 40,
                              initEffects = new effectData[] // сет пропсов по умолчанию для всех стран сцены
                                                             // их считаем за норму, к ним демпингуем
                              {
                                new effectData{ var = "relA", val = 0.33f },
                                new effectData{ var = "relB", val = 0.33f },
                                new effectData{ var = "relC", val = 0.33f },
                                new effectData{ var = "crown", val = 0.1f },
                                new effectData{ var = "agr", val = 0.5f },
                                new effectData{ var = "armor", val = 0.5f },
                                new effectData{ var = "econ", val = 1f },
                              },
                              noiseEffects = new effectData[] // коэффициент шума от 0 до 1 к стандартному
                                                              // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .5f },
                                new effectData{ var = "relB", val = .5f },
                                new effectData{ var = "relC", val = .5f },
                                new effectData{ var = "crown", val = .5f },
                                new effectData{ var = "agr", val = .1f },
                                new effectData{ var = "armor", val = 0.1f },
                                new effectData{ var = "econ", val = 0f },
                              },
                              dempEffects = new effectData[] // коэффициент демпинга от 0 до 1 к стандартному
                                                             // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .5f },
                                new effectData{ var = "relB", val = .5f },
                                new effectData{ var = "relC", val = .5f },
                                new effectData{ var = "crown", val = 1f },
                                new effectData{ var = "agr", val = .5f },
                                new effectData{ var = "armor", val = 1f },
                                new effectData{ var = "econ", val = 1f },
                              },
                            } ,
                     new scCntData
                            { cntID = 1,
                              capPlace = new Vector3(19, 1, -20),
                              areaMin = 20,
                              areaNorm = 60,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relA", val =.5f },
                              },
                            } ,
                      new scCntData
                            { cntID = 2,
                              capPlace = new Vector3(19, 10, -29),
                              areaMin = 20,
                              areaNorm = 120,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relC", val = .5f },
                              },

                            },
                      new scCntData
                            { cntID = 3,
                              capPlace = new Vector3(10, 10, -20),
                              areaMin = 20,
                              areaNorm = 120,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relB", val = .5f },
                              },

                            }
                     },
            conds = new scConditionsData[]
                    { new scConditionsData
                        {
                            par = "xp",
                            val = 1000,
                        }
                    },
            statPanEls = new string[]
                    {
                      "rel",
                      "crown",
                      "agr",
                      //"armor",
                      //"econ",
                    },
            toolPanEls = new string[]
                    {
                      //"RelA+1",
                      "RelA+3",
                      "RelA+5",
                      //"RelB+1",
                      "RelB+3",
                      "RelB+5",
                      //"RelC+1",
                      "RelC+3",
                      "RelC+5",
                      "Crown+5",
                      "Crown-5",
                      "Agr+5",
                      "Agr-5",
                    },
            taskPanGlyphNum = 1,
        };

        Glob.sceneVals.Add(sv);

        sv = new SceneVals
        {
            title = "Туториал. Глава 4. Вооруженность",
            annot = "Старый Маркус усмехается: <br>– Вот ты говоришь: «Власть и оружие», – мол, на этом зиждится война. " +
                    "<line-height=175%><br><line-height=100%>" +
                    "Запомни: власть политика – иллюзия. Врач в хирургическом кабинете имеет истинную власть над твоим телом. " +
                    "Учительница в школе – истинную власть над твоим умом. Смазливая красавица – истинную власть над твоим сердем. "+
                    "Но никто из них не возглавляет войн. Власть диктатора призрачна, оттого он в ней так ненасытен. " +
                    "<line-height=175%><br><line-height=100%>" +
                    "А оружие, да, ты прав: без оружия войны быть не может. " +
                    "Поэтому все и идут к старику Маркусу. Маркус был когда-то никем, но теперь он знает о войне несколько простых истин.",
            goal = "\u2666 Когда параметр вооруженности "+
                    "<size=120%><b><color=#70A400>[\u25a0]</color></b></size>" +
                    "падает до минимума, боевые действия невозможны. " +
                    "Действуй в этот момент – и война не прекратится.",
            // activeCntNum = 3,
            cntVals = new scCntData[]
                    { new scCntData
                            { cntID = 0,
                              capPlace = new Vector3(0, 0, 0),
                              areaMin = 20,
                              areaNorm = 200,
                              areaMax = 40,
                              initEffects = new effectData[] // сет пропсов по умолчанию для всех стран сцены
                                                             // их считаем за норму, к ним демпингуем
                              {
                                new effectData{ var = "relA", val = 0.33f },
                                new effectData{ var = "relB", val = 0.33f },
                                new effectData{ var = "relC", val = 0.33f },
                                new effectData{ var = "crown", val = 0.1f },
                                new effectData{ var = "agr", val = 0.5f },
                                new effectData{ var = "armor", val = 0.5f },
                                new effectData{ var = "econ", val = 1f },
                              },
                              noiseEffects = new effectData[] // коэффициент шума от 0 до 1 к стандартному
                                                              // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .5f },
                                new effectData{ var = "relB", val = .5f },
                                new effectData{ var = "relC", val = .5f },
                                new effectData{ var = "crown", val = .5f },
                                new effectData{ var = "agr", val = .1f },
                                new effectData{ var = "armor", val = 0.1f },
                                new effectData{ var = "econ", val = 0f },
                              },
                              dempEffects = new effectData[] // коэффициент демпинга от 0 до 1 к стандартному
                                                             // для всех на сцене
                              {
                                new effectData{ var = "relA", val = .5f },
                                new effectData{ var = "relB", val = .5f },
                                new effectData{ var = "relC", val = .5f },
                                new effectData{ var = "crown", val = 1f },
                                new effectData{ var = "agr", val = .5f },
                                new effectData{ var = "armor", val = .5f },
                                new effectData{ var = "econ", val = 1f },
                              },
                            } ,
                     new scCntData
                            { cntID = 1,
                              capPlace = new Vector3(19, 1, -20),
                              areaMin = 20,
                              areaNorm = 60,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relA", val =.5f },
                              },
                            } ,
                      new scCntData
                            { cntID = 2,
                              capPlace = new Vector3(19, 10, -29),
                              areaMin = 20,
                              areaNorm = 120,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relC", val = .5f },
                              },

                            },
                      new scCntData
                            { cntID = 3,
                              capPlace = new Vector3(10, 10, -20),
                              areaMin = 20,
                              areaNorm = 120,
                              areaMax = 40,
                              initEffects = new effectData[]
                              {
                                  new effectData { var = "relB", val = .5f },
                              },

                            }
                     },
            conds = new scConditionsData[]
                    { new scConditionsData
                        {
                            par = "xp",
                            val = 1000,
                        }
                    },
            statPanEls = new string[]
                    {
                      "rel",
                      "crown",
                      "agr",
                      "armor",
                      //"econ",
                    },
            toolPanEls = new string[]
                    {
                      //"RelA+1",
                      //"RelA+3",
                      "RelA+5",
                      //"RelB+1",
                      //"RelB+3",
                      "RelB+5",
                      //"RelC+1",
                      //"RelC+3",
                      "RelC+5",
                      "Crown+5",
                      "Crown-5",
                      "Agr+5",
                      "Agr-5",
                      "Armor+5",
                    },
            taskPanGlyphNum = 1,
        };

        Glob.sceneVals.Add(sv);
        foreach (var sc in Glob.sceneVals)
	    {
            sc.autoShowScenePan = true;
            print("sc=" + sc.title + "countr=" + sc.cntVals.Length);
	    }
    }
}

