using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class SceneBut : MonoBehaviour
{
    [SerializeField] TMP_Text ButTxt;
    [SerializeField] int mySceneNum;
    void Start()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(ButOnClick);
    }

    public void Init(SceneVals sc, int num)
    {
        mySceneNum = num;
        ButTxt.text = sc.title;
    }
    void ButOnClick()
    {
        Glob.GMan.SceneButtonClick(mySceneNum);
    }
}
