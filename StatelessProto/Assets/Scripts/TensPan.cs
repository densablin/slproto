using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TensPan : MonoBehaviour
{
    [SerializeField] RectTransform TensElPanPref;
    [SerializeField] TensElPan[] TensElList = new TensElPan[6];

    // Start is called before the first frame update
    void Awake()
    {
        Glob.TensPan = this;
    }
    private void Start()
    {
        TensElPanPref.gameObject.SetActive(false);
        TensElList = new TensElPan[6];
        for (int i = 0; i < 6; i++)
        {
            GameObject tElp = Instantiate(TensElPanPref.gameObject, transform);
            TensElList[i] = tElp.GetComponent<TensElPan>();
            TensElList[i].StopView();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartView(HexCell hc)
    {
        if (!hc.IsFrontierCell()) return;

        int tenNum = 0;
        Dictionary<int, int> cntInTens = new Dictionary<int, int>();
        foreach (var nh in hc.NHCell)
        {
            if (hc != null) 
            {
                if (nh.myCountryID == hc.myCountryID) continue;
                if (nh.myCountryID == 0) continue;

                cntInTens.TryAdd(nh.myCountryID, nh.myCountryID);
            }
        }
        foreach (var cnt in cntInTens)
        {
            // print("Start View Tens cA=" + hc.myCountryID + " cB=" + cnt.Value);
            TensElList[tenNum].StartView(hc.myCountryID, cnt.Value);
            tenNum++;
        }
        for (int i = cntInTens.Count; i < 6; i++)
        {
            TensElList[i].StopView();
        }
    }
}
