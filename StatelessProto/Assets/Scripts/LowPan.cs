using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LowPan : MonoBehaviour
{
    public Scrollbar scBar;
    public Button LeftBut;
    public Button RightBut;

    bool _enabl;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (_enabl != scBar.isActiveAndEnabled)
        {
            _enabl = scBar.isActiveAndEnabled;
            LeftBut.gameObject.SetActive(_enabl);
            RightBut.gameObject.SetActive(_enabl);

            // print(scBar.isActiveAndEnabled);
        }
    }
    public void MoveScroll(float m)
    {
        float k = .2f;
        scBar.value += k * m;
    }
}
