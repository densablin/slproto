using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UserControl : MonoBehaviour
{
    private void Awake()
    {
        Glob.UCont = this;
    }
    
    void Update()
    {

        if (Input.GetButtonDown("Fire1") && !EventSystem.current.IsPointerOverGameObject()) 
        {
            HexCell hc = Glob.MField.CastCellUnderCursor();
            if (hc != null)
            {
                if (Glob.Countries[hc.myCountryID].isActive) 
                { 
                    Glob.ShowStatCountryID = hc.myCountryID;

                    // �������� ������������ ����� �� ������
                    hc.StartClick();

                    // ���� ���������, ���������� ������ ����������
                    Glob.TensPan.StartView(hc);

                   // print("HC="+ hc.name + " isFr=" + " in List"
                   //    + hc.IsFrontierCell() +" "+ Glob.Countries[hc.myCountryID].frCells.Contains(hc));
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Glob.ScenePan.ButOnClick();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            print("S-key was pressed");
            Glob.StatPan.ShowAll();
        }
    }
    
}
