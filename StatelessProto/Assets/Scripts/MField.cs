using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MField : MonoBehaviour
{
    public GameObject CellPref;
    public Gradient mapCols; // ������������� ����� �����
    public Gradient frontCols; // ���������� ����� ��������
    public Gradient transCols; // ������ � ���������
    public AnimationCurve reactCols; // ��������� ������� ����� ������ ������ ����� ������ ������
    public AnimationCurve clickCols; // ��������� ������� ����� ������ �� ����
    public Color inactiveFrontCol; // ���� ����� �������� ����� ����������� ��������
    public Color capIconCol; // ���� ������ �������
    public float transitTime = .5f;
    public Color cnt0col = Color.cyan;

    public Sprite[] waterSprs;
    public Sprite[] activeCntSprs;
    public Sprite[] enActiveCntSprs;

    private void Awake()
    {
        Glob.MField = this;
    }
    public void InitField(int scn)
    {
        // Glob.totalCountNum = Glob.sceneVals[scn].activeCntNum + 5; // ������� ���� 4 ������
        Glob.totalCountNum = Glob.sceneVals[scn].cntVals.Length + 4; // ������� ���� 4 ������
        InitCountries(Glob.sceneVals[scn]);
        Glob.Calc.CalcRelateMx();

        // ������ �� ��� ������������� ������� ����� ������ ����
        FillField();

        RevisionAllCountries();
    }

    void Update()
    {
        if (Glob.dataIsReady)
        {
            // ��������� ������� ���������� �������� �����
            foreach (var cnt in Glob.Countries)
            {
                if (cnt.reAccept && Time.time >= cnt.reAcceptTime1) cnt.reAccept = false;
            }
        }

    }

    public void StartReactCountry(int cntNum)
    {
        Glob.Countries[cntNum].reAcceptTime0 = Time.time;
        Glob.Countries[cntNum].reAcceptTime1 = Time.time + Glob.reAcceptTime;
        Glob.Countries[cntNum].reAccept = true;
    }
    void InitCountries(SceneVals scn)
    {
        // �������� ������
        // Glob.Countries = new Country[scn.totalCntNum];
        Glob.Countries = new Country[Glob.totalCountNum]; 

        CProps prop = Calc.ConvertEffectToProps(scn.cntVals[0].initEffects);
        CProps dempProp = Calc.ConvertEffectToProps(scn.cntVals[0].dempEffects);
        CProps noiseProp = Calc.ConvertEffectToProps(scn.cntVals[0].noiseEffects);

        // PrintProps(prop);

        // ��������� ������ �������� �� Data ������� ������
        for (int i = 0; i < Glob.Countries.Length; i++)
        {
            Glob.Countries[i] = new Country
            {
                cnID = i,
                isActive = false,
                myCells = new List<HexCell>(),
                frCells = new List<HexCell>(),
                prop = prop,
                preProp = prop,
                dempProp = dempProp,
                noiseProp = noiseProp,
                cellCol = InitCellColor(i, false),
                areaMin = scn.cntVals[0].areaMin,
                areaNorm = scn.cntVals[0].areaNorm,
                areaMax = scn.cntVals[0].areaMax,
                capitalCellId = 0,
            };
            
        }

        // ������ ������ 4 ����� �������� ������ ������� �� �����

        Glob.Countries[Glob.Countries.Length - 4].capitalCellId = Glob.CalcCellId(0, 0, 0);
        Glob.Countries[Glob.Countries.Length - 3].capitalCellId = Glob.CalcCellId(0, 29, -29);
        Glob.Countries[Glob.Countries.Length - 2].capitalCellId = Glob.CalcCellId(29, 15, -44);
        Glob.Countries[Glob.Countries.Length - 1].capitalCellId = Glob.CalcCellId(29, -14, -15);


        // ������ ������, ��� ������� � �����
        foreach (var cv in scn.cntVals)
        {
            if (cv.cntID == 0) continue ;// ������� ��� ����� � �������� ��������

            if (cv.cntID > Glob.Countries.Length - 1)
            {
                print("Data: ����� � �������� ������=" + cv.cntID 
                    + " ������ ��������� totalCntNum=" + Glob.totalCountNum);
                continue;
            }

            prop = Calc.IncludeEffectToProps(cv.initEffects, Glob.Countries[cv.cntID].prop);
            dempProp = Calc.IncludeEffectToProps(cv.dempEffects, Glob.Countries[cv.cntID].dempProp);
            noiseProp = Calc.IncludeEffectToProps(cv.noiseEffects, Glob.Countries[cv.cntID].noiseProp);

            // PrintProps(prop);

            Glob.Countries[cv.cntID] = new Country
            {
                cnID = cv.cntID,
                isActive = true,
                myCells = new List<HexCell>(),
                frCells = new List<HexCell>(),
                prop = prop,
                preProp = prop,
                dempProp = dempProp,
                noiseProp = noiseProp,
                cellCol = InitCellColor(cv.cntID, true),
                areaMin = cv.areaMin,
                areaNorm = cv.areaNorm,
                areaMax = cv.areaMax,
                capitalCellId = Glob.CalcCellId((int)cv.capPlace[0], (int)cv.capPlace[1], (int)cv.capPlace[2]),
            };
        }
        
    }
       
    void PrintProps(CProps props)
    {
        print(props.relA + " " + props.relB + " " + props.relC);
    }
    Color InitCellColor(int n, bool isActive )
    {
        if (n == 0) return cnt0col;

        float k = .36f; //  "�������� �����������" ����������� �� �������� ������
        float sh = 0; //Random.Range(-.05f,.05f); //  ��������� ������ ��� "��������� ������������"
        float r = ((n-1) * k - sh) % 1f;
        Color col = mapCols.Evaluate(r);

        //if (isActive)
        //{
        //    print("ColorCycle: n=" + n + " sh=" + sh + " r=" + r);
        //}

        // ��� ���� �������� ��� ������� �������������� ���������� ������
        if (!isActive)
        {
            //Color fadeCol = new Color(0.1f, 0.15f, 0.25f);
            //col = Color.Lerp(col, fadeCol, .8f);

            Color.RGBToHSV(col,out float h, out float s, out float v);
            col = Color.HSVToRGB(h, s*.3f, v*.5f);
            // col *= .75f;
        }
        return col;
    }
    
    public void CreateCells(int FieldH, int FieldW)
    {
        Glob.Cells = new Dictionary<int, HexCell>();

        for (int w = 0; w < FieldW; w++)
        {
            for (int h = 0; h < FieldH; h++)
            {
                int r = h;
                int q = w - (h / 2); // (h / 2) -- ������� ��� ���������� ������������� �.�. int
                int s = -q - r;

                HexCell hGo = CreateCell(r, q, s);
                Glob.Cells.Add(hGo.uId, hGo);
            }
        }

        // ��������� ��� ������, ����������� ���� �������
        SetCellsNH();

    }
    HexCell CreateCell(int r, int q, int s)
    {
        GameObject go = Instantiate(CellPref, transform);
        HexCell hc = go.GetComponent<HexCell>().Init(r,q,s,0);

        return hc;
    }


    void SetCellsNH()
    {
        foreach (var hcPair in Glob.Cells)
        {
            // print("hx=" + hl.Value.gameObject.name);

            int nr = hcPair.Value.r;
            int nq = hcPair.Value.q;
            int ns = hcPair.Value.s;

            for (int j = 0; j <= 5; j++)
            {
                // ������
                // 0 - r,q+1,s-1 ->
                // 1 - r-1,q+1,s \.
                // 2 - r-1,q,s+1 ./
                // 3 - r,q-1,s+1 <-
                // 4 - r+1,q-1,s ^\
                // 5 - r+1,q,s-1 /^

                switch (j)
                {
                    case 0:
                        nr = hcPair.Value.r;
                        nq = hcPair.Value.q + 1;
                        ns = hcPair.Value.s - 1;

                        break;
                    case 1:
                        nr = hcPair.Value.r - 1;
                        nq = hcPair.Value.q + 1;
                        ns = hcPair.Value.s;
                        break;
                    case 2:
                        nr = hcPair.Value.r - 1;
                        nq = hcPair.Value.q;
                        ns = hcPair.Value.s + 1;
                        break;
                    case 3:
                        nr = hcPair.Value.r;
                        nq = hcPair.Value.q - 1;
                        ns = hcPair.Value.s + 1;
                        break;
                    case 4:
                        nr = hcPair.Value.r + 1;
                        nq = hcPair.Value.q - 1;
                        ns = hcPair.Value.s;
                        break;
                    case 5:
                        nr = hcPair.Value.r + 1;
                        nq = hcPair.Value.q;
                        ns = hcPair.Value.s - 1;
                        break;

                }
                
                int uId = Glob.CalcCellId(nr, nq, ns);
                Glob.Cells.TryGetValue(uId, out HexCell nhCell);
                hcPair.Value.NHCell[j] = nhCell;
            }
        }
    }

    void FillField()
    {
        foreach (var hc in Glob.Cells)
        {
            hc.Value.ResetCell(); // ���� ���� � ����
        }

        int maxArea = 0;
        foreach (var cn in Glob.Countries)
        {
            // ������� ���������� � ����� ������� �������
            maxArea = Mathf.Max(maxArea, cn.areaNorm);

            // ����������� ��������� ������
            Glob.Cells.TryGetValue(cn.capitalCellId, out HexCell hcCell);
            if (hcCell != null)
            {
                hcCell.AssignToCountrySimple(cn.cnID);
            }

        }

        for (int i = 1; i < maxArea; i++) // �������� � 1, �.�. �� ����� ��������� ������ ��� ����
        {
            // �� ������� ��������� �� ����� ������ ������ ������
            foreach (var cn in Glob.Countries)
            {
                if (cn.cnID == 0) continue; // ������� ������ - �������, �� � ������

                if (i < cn.areaNorm)
                {
                    // �������� ������ �����, ��, ��� ������ 0
                    OccupateCellSimple(cn.cnID, 0);
                }
            }
        }
        

        // ������ �� ����, ��������� ������������ ������ ������
        foreach (var cl in Glob.Cells)
        {
            cl.Value.UpView();
        }
    }
    public void UpViewAllFrontCells()
    {
        foreach (var cn in Glob.Countries)
        {
            if (!cn.isActive) continue;
            foreach (var hc in cn.frCells)
            {
                hc.UpView();
            }
        }
    }

    void RevisionAllCountries() // �������� �� �������� ������ ���� ������ �� ������ ������
    {
        // �������� ��� ������, ������� �� ������ � �������
        for (int i = 0; i < Glob.Countries.Length; i++)
        {
            Glob.Countries[i].myCells = new List<HexCell>();
            Glob.Countries[i].frCells = new List<HexCell>();
        }

        foreach (var hx in Glob.Cells) // ��������� �� ���� ������� ����, ����������� ������ 
                                       // � ������ ��������������� �����
        {
            Glob.Countries[hx.Value.myCountryID].myCells.Add(hx.Value);
        }
        foreach (var hx in Glob.Cells) // ��������� �� ���� ������� ����, 
                                       // ���������, ����� �� ��� �����������
        {
            //if (hx.Value.IsFrontierCell())
            // Glob.Countries[hx.Value.myCountryID].frCells.Add(hx.Value);
            hx.Value.UpView();
        }


        for (int i = 0; i < Glob.Countries.Length; i++)
        {
            print("Cnt=" + i + " cells="
                + Glob.Countries[i].myCells.Count
                + " frCells=" + Glob.Countries[i].frCells.Count
                + " cap hcID" + Glob.Countries[i].capitalCellId
                + " cap hc" + Glob.Cells[Glob.Countries[i].capitalCellId].name
                ) ;
        }

    }
    void RevisionOneCountry(int cnID) // �������� �� �������� ������ ���� ������ �� ���������� ������
    {
        Glob.Countries[cnID].myCells = new List<HexCell>();
        Glob.Countries[cnID].frCells = new List<HexCell>();

        foreach (var hx in Glob.Cells) // ��������� �� ���� ������� ����, ���� ������ � ���������� ������
        {
            if (hx.Value.myCountryID == cnID) hx.Value.AssignToCountry(cnID);
        }
    }
    
    

    public HexCell CastCellUnderCursor()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity, 255);
        if (hit)
        {
            // Debug.Log("hit col=" + hit.collider.name);
            HexCell hc = hit.collider.gameObject.GetComponent<HexCell>();
            return hc;
        }
        return null;
    }

    public HexCell GetRandomFrontirCell(int myCnID, int otherCnID) 
        // �������� ��������� ������ ���������� � ���������� ������� (���� �����)
    {
        List<HexCell> FrWithCnID = new List<HexCell>();
        foreach (var hc in Glob.Countries[myCnID].frCells)
        {
            if (hc.HasFrWithCnID(otherCnID))
            {
                FrWithCnID.Add(hc);
            }

        }

        if (FrWithCnID.Count > 0) 
        { 
            int r = Random.Range(0, FrWithCnID.Count);
            return FrWithCnID[r];
        } else
        {
            // print("cant find FrWithCnID myID="+ myCnID + " other cnID=" + otherCnID);
            return null;
        }
    }
    
    public void OccupateCellSimple(int myCnId, int otherCnId) // ������ �������� (�� �����������) 
                                                              // ������ �������� ������
    {
        HexCell myHc = GetRandomFrontirCell(myCnId, otherCnId);
        if (myHc == null) return;

        HexCell otherHc = myHc.GetRandomNHCell(otherCnId);
        if (otherHc == null) return;

        // Glob.Countries[myCnId].myCells.Add(otherHc);
        otherHc.AssignToCountry(myCnId);
    }

    public Sprite GetRandomHCellSprite(int cnId)
    {
        if (cnId == 0)
        {
            return RandomSpriteFromList(waterSprs);
        } else
        {
            if (Glob.Countries[cnId].isActive)
            {
                return RandomSpriteFromList(activeCntSprs);
            }
            else
            {
                return RandomSpriteFromList(enActiveCntSprs);
            }
        }

    }
    public float GetRandomHCellAngle(int cnId)
    {
        if (cnId == 0)
        {
            return 0;
        }
        else
        {
            return RandomAngleForHex();
        }
    }
    
    Sprite RandomSpriteFromList(Sprite[] sprArr)
    {
        if (sprArr.Length == 0) return null;

        return sprArr[Random.Range(0, sprArr.Length)];
    }
    float RandomAngleForHex()
    {
        float[] agnArr = new float[]
        {
            0, 60, 120,
            180, 240, 300
        };

        return agnArr[Random.Range(0, agnArr.Length)];
    }
}

/*
 * if (!EventSystem.current.IsPointerOverGameObject())
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity, 255);
            if (hit)
            {
                // Debug.Log("hit col=" + hit.collider.name);
                MapDecorEl mel = hit.collider.gameObject.GetComponent<MapDecorEl>();
                return mel;
            }
        }
 */