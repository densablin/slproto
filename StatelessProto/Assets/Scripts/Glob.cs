using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glob
{

    public static MField MField;
    public static Calc Calc;
    public static GameMan GMan;
    public static UserControl UCont;
    public static AgentMan AgMan;
    public static StatPan StatPan;
    public static DataGen DGen;
    public static TensPan TensPan;
    public static ScenePan ScenePan;
    public static TaskPan TaskPan;
    public static TickButsPan TickButsPan;

    public static bool dataIsReady = false;

    public static int sceneNum = 0; // ����� ������� �����

    public static float HexR = .50f; // ������ �����
    public static float HexH = HexR * 2;
    public static float HexW = Mathf.Sqrt(3) * Glob.HexR / 2;
    public static float SpaceVert = HexH * 0.75f;
    public static float SpaceHor = HexW * 2;

    // ������� ���������������� ���� ������
    public static int FieldV = 30;
    public static int FieldH = 30;

    public static Dictionary<int, HexCell> Cells; // ��� ������ �����

    public static int totalCountNum = 3; // ������� ������� �����
    public static Country[] Countries; // ��� ������
    public static CRelation [,] RelateMx; // ��������� ����� ��������

    public static int ShowStatCountryID = 0; // ������, ������� ���������� � ����������

    public static int totalMoney;

    public static float reAcceptTime = 1f; //  ����� �� ����������� ������� ������ ������ �� ����� ������
    public static float clickTime = .5f; //  ����� �� ����������� ������� ������ �� ����

    public static Dictionary<int, Sprite> glyphLib;
    public static List<SceneVals> sceneVals;

    public static int MaxR = 100;
    public static int CalcCellId(int r, int q, int s)
    {
        return r + (q * MaxR);
    }
    public delegate void TickDlg();
    public static TickDlg DoTick;
}

public class Country
{
    public int cnID; // ID ������
    public bool isActive; // ��������� �� ������ � ������� ��������

    public CProps prop; // ��������, ����������� � ���.������ (����������)

    public CProps preProp; // ��������, ����������� � ���.������ (���������������)
    public CProps dempProp; // ������������ �������� ���������� ������
    public CProps noiseProp; // ������������ ���� ���������� ������
    public CProps bufAdds; // �������� ���������� ���������� ������� ��������
    public CProps bufNorms; // �������� ��������� ���� ������� ��������

    public Color cellCol; // ���� ������ �� ����
    public int capitalCellId; // ������ ������������ ������

    // ������� ������ � �������
    public int areaMin; // ��������, ���� �������� ������ �������� ���������� � �������������
    public int areaNorm; // �������� ����������� "����������" �������
    public int areaMax; // ��������, ���� �������� ������ �������� ������ ����������

    public float agrCoolDown = 0; // ������� "������ ����������" ��� ����������� ���������

    public List<HexCell> myCells; // ������ ���� ������ ������
    public List<HexCell> frCells; // ������ ���� ���������� ������
    public int transCellCount = 0;

    // ����������� � �����
    public bool reAccept = false; // ����������� "������" ���������
    public float reAcceptTime0;
    public float reAcceptTime1;

    

}
public class SceneVals
{
    public string title;
    public string annot;
    public string goal;
    // public int activeCntNum;
    public scCntData[] cntVals;
    public scConditionsData[] conds;
    public string[] toolPanEls;
    public string[] statPanEls;
    public bool autoShowScenePan;
    public int taskPanGlyphNum;
    // public int nextSceneNum;

}
public class scConditionsData
{
    public string par;
    public float val;
}
public struct scCntData
{
    public int cntID;
    public Vector3 capPlace;
    public effectData[] initEffects;
    public effectData[] dempEffects;
    public effectData[] noiseEffects;
    public int areaMin;
    public int areaNorm;
    public int areaMax;
}
public class CRelation
{
    public float Tens;
}

[System.Serializable]
public struct CProps { 
    public float relA; // ��������� A
    public float relB; // ��������� B
    public float relC; // ��������� C
    public float agr; // ������� �������������
    public float crown; // ������� ����������
    public float armor; // �������������
    public float econ; // ������������� ����������
}
enum panState { hide, fadeIn, fadeOut, show }
