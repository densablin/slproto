using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenePan : MonoBehaviour
{

    [SerializeField] GameObject SceneButPref;
    [SerializeField] Vector2 pos0;
    [SerializeField] Vector2 pos1;
    [SerializeField] float fadeTime;
    [SerializeField] AnimationCurve fInCur;
    [SerializeField] AnimationCurve fOutCur;
    RectTransform myRT;

    float time0;
    float time1;
    panState myState = panState.hide;

    private void Awake()
    {
        Glob.ScenePan = this;
    }
    public void Start()
    {
        // print("stssssssssssss");
        myRT = gameObject.GetComponent<RectTransform>();
        myRT.anchoredPosition = pos0;
    }
    public void StartView()
    {
        gameObject.SetActive(true);
        // 
        if (myState == panState.hide || myState == panState.fadeOut)
        {
            time0 = Time.time;
            time1 = Time.time + fadeTime;
            myState = panState.fadeIn;
        }
    }
    public void StopView()
    {
        if (myState == panState.show || myState == panState.fadeIn)
        {
            time0 = Time.time;
            time1 = Time.time + fadeTime;
            myState = panState.fadeOut;
        }
    }

    void Update()
    {
        if (Time.time >= time1) SetNexState();

        float k = myState switch
        {
            panState.hide => 0,
            panState.fadeIn => fInCur.Evaluate(Mathf.InverseLerp(time0, time1, Time.time)),
            panState.show => 1,
            panState.fadeOut => fOutCur.Evaluate(Mathf.InverseLerp(time0, time1, Time.time)),
            _ => 1,
        };
        myRT.anchoredPosition = Vector2.Lerp(pos0, pos1, k);

    }
    void SetNexState()
    {
        switch (myState)
        {
            case panState.fadeIn:
                myState = panState.show;
                break;
            case panState.fadeOut:
                myState = panState.hide;
                gameObject.SetActive(false);
                break;
        }
    }
    public void Init()
    {
        for (int i = 0; i < Glob.sceneVals.Count; i++)
        {
            GameObject go = Instantiate(SceneButPref, SceneButPref.transform.parent);
            go.GetComponent<SceneBut>().Init(Glob.sceneVals[i],i);
            go.SetActive(true);
        }
    }
    public void ButOnClick()
    {
        if (myState == panState.hide)
        {
            StartView();
            return;
        }
        if (myState == panState.show)
        {
            StopView();
            return;
        }


    }
}
