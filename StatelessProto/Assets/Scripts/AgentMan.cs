using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[System.Serializable]
public class Agent
{
    public string agID;
    public int iconPicA;
    public int iconPicB;
    public int infoPic;
    public Color baseCol;
    public effectData[] effect;
    public int cartPic;
    public string annotation;
    public int price;
    public string type;
    public CProps prop;
}

[System.Serializable]
public struct effectData
{
    public string var;
    public float val;
}

[System.Serializable]
public struct glyphEl
{
    public int id;
    public Sprite spr;
}

public class AgentMan : MonoBehaviour
{
    public bool hilIsOn; // ����� �� ������������ ������ (����� ��� ���)
    public int hilCountID; // ����� ������ ������������
    public Agent hilAgent;

    public AgInfoPan InfoPan;
    public RectTransform CursorPan;
    public Image CurPicA;
    public Image CurPicB;
    public RectTransform ToolPan;
    public GameObject AgPanPref;
    public GameObject ForbidSign;
    //public glyphEl[] glyphLib;
    //public List<Agent> AgentLib;
    private void Awake()
    {
        Glob.AgMan = this;
        Glob.DoTick += DoTick;
    }
    void Start()
    {
        // ReadAgentLib();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void DoTick()
    {
        // ��� ��������� ���������� ����� �������
        if (hilIsOn)
        {
            ForbidSign.SetActive(isForbidAgent(hilAgent, hilCountID));
            return;
        }
        ForbidSign.SetActive(false);
    }
    bool isForbidAgent(Agent ag, int cnId)
    {
        bool isForb = false;
        switch (ag.type)
        {
            case "armor":// ���� ����� ��� ����������
                isForb = !Glob.Calc.CanPutArmor(ag, cnId);
                break;
        }
        return isForb;
    }
    public void AgentStartDrag(Agent ag)
    {
        ActivateCursor(ag);
    }
    public void AgentOnMove(Agent ag) // ����� ����� ���� � �-�-� � ��������� � ������ 
    {
        CursorPan.position = Input.mousePosition;
        hilIsOn = false; // �� ��������� �� ������������ ������

        Country cnt = FindCountryUnderCursor();
        if (cnt != null)
        {
            // ����� ����������� ������, ��� ������� ��������� ���������
            hilCountID = cnt.cnID;
            hilAgent = ag;
            hilIsOn = true;

            // � ���������� � �������� ����������
            Glob.ShowStatCountryID = cnt.cnID;
        }
    }
    public void AgentEndDrag(Agent ag)
    {
        hilIsOn = false;
        
        // ������� �������� � �������
        ReleaseCursor();

        Country cnt = FindCountryUnderCursor();
        if (cnt != null)
        {
            Glob.ShowStatCountryID = cnt.cnID;

            if (!isForbidAgent(ag, cnt.cnID)) { 

                Glob.MField.StartReactCountry(cnt.cnID);

                // ����������, ������� ������ ������ � ����� ������
                Glob.Calc.AddAgentProps(ag, cnt.cnID);

                // � �������� ����� �� �����
                Glob.GMan.ChangeMoney(-ag.price);
            }
        }

    }

    
    public void AgentIsSelect(Agent ag)
    {
        InfoPan.ShowPan(ag);
    }

    Country FindCountryUnderCursor() // �������� �������� ������ ��-��� �������
    {
        // ��������� �� ��������� UI - ������ ��� ��� ���
        // ���������� ������
        if (EventSystem.current.IsPointerOverGameObject()) return null;

        HexCell hc = Glob.MField.CastCellUnderCursor();

        // ������ ���� ������, ���������� ������
        if (hc == null) return null;

        // ������ �����, �� ��� ��������� -- �� ����������
        if (!Glob.Countries[hc.myCountryID].isActive) return null;
        return Glob.Countries[hc.myCountryID];

    }

    void ActivateCursor(Agent ag)
    {
        CursorPan.GetComponent<Image>().color = ag.baseCol;
        CurPicA.sprite = Glob.glyphLib[ag.iconPicA];
        CurPicB.sprite = Glob.glyphLib[ag.iconPicB];
        CursorPan.gameObject.SetActive(true);
    }
    void ReleaseCursor()
    {
        CursorPan.gameObject.SetActive(false);
    }

    public void GenerateAgPan(string agID)
    {
        Agent ag;
        if (Glob.DGen.AgentLib.TryGetValue(agID, out ag))
        {
            GameObject aPanGO = Instantiate(AgPanPref, ToolPan.transform);
            aPanGO.GetComponent<AgentPan>().Init(ag);


        }
        else
        {
            print("Cant create AgPan from " + agID);
        };
    }
    public void InitToolPan(SceneVals scn)
    {
        // ������ ������� �������
        RemoveOldPanEls();

        // ������ ����� �����, ����� ��� �� Data ������ �����
        foreach (var el in scn.toolPanEls)
        {
            GenerateAgPan(el);
        };
    }

    void RemoveOldPanEls()
    {
        for (int i = 0; i < ToolPan.transform.childCount; i++)
        {
            GameObject go = ToolPan.transform.GetChild(i).gameObject;
            if (go == AgPanPref) continue;
            go.SetActive(false);
            Destroy(go);
        }
    }
}
