using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AgentPan : MonoBehaviour
{
    public string myAgentID;
    Agent myAgent;
    public Image bgImg;
    public Image picAImg;
    public Image picBImg;

    public void Init(Agent ag)
    {
        picAImg.sprite = Glob.glyphLib[ag.iconPicA];
        picBImg.sprite = Glob.glyphLib[ag.iconPicB];
        bgImg.color = ag.baseCol;
        myAgent = ag;
        gameObject.name = "Agent (" + ag.agID + ")";
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartDrag()
    {
        Glob.AgMan.AgentStartDrag(myAgent);
    }

    public void DragMove()
    {
        Glob.AgMan.AgentOnMove(myAgent);
    }
    public void EndDrag()
    {
        Glob.AgMan.AgentEndDrag(myAgent);
    }
    public void isClick()
    {
        Glob.AgMan.AgentIsSelect(myAgent);
    }

}
