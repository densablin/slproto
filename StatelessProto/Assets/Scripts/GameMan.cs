using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMan : MonoBehaviour
{
    public float tickPeriod = 0.2f;
    public enum ts { stop, X1, X2, X3 };
    public ts tickSpeed;
    public ts lastTickSpeed;
    float time1;
    float time0;
    [SerializeField] TaskPan tPan;
    [SerializeField] ScenePan sPan;

    public int cellPrice = 1;
    private void Awake()
    {
        Glob.GMan = this;

        // ��� ����������� ������ ������, �������� ��� �������������
        tPan.gameObject.SetActive(true);
        sPan.gameObject.SetActive(true);

        tickSpeed = ts.X1;
    }

    void Start()
    {
        Glob.DGen.InitReadLibs(); // ��������� ��� ���������� �� DataGen
        Glob.ScenePan.Init();
        // Glob.ScenePan.Start();

        // ������ ��� ������ ���� ��� � �������� ��� ���� ����
        Glob.MField.CreateCells(Glob.FieldH, Glob.FieldV);
        InitScene(0);
        Glob.ScenePan.StartView();
    }

    void Update()
    {
        // ������� ��� ����
        if (Time.time >= time1 && tickSpeed != ts.stop && Glob.dataIsReady)
        {
            SetTimer(tickSpeed);
            Glob.DoTick();
        }
    }
    void SetTimer(ts t)
    {
        time0 = Time.time;
        switch (t)
        {
            case ts.stop:
                break;
            case ts.X1:
                time1 = Time.time + tickPeriod * 3;
                break;
            case ts.X2:
                time1 = Time.time + tickPeriod * 2;
                break;
            case ts.X3:
                time1 = Time.time + tickPeriod * 1;
                break;
        }
    }
    public void InitScene(int scNum)
    {
        Glob.dataIsReady = false;
        Glob.sceneNum = scNum;

        Glob.MField.InitField(Glob.sceneNum); // ������� ������, ������ ���� ������

        Glob.Calc.InitCalc(Glob.sceneVals[Glob.sceneNum]); // ����������� ����������� � ������� �����

        // ���������� ���������� ������ ��������� ������
        Glob.ShowStatCountryID = Glob.sceneVals[Glob.sceneNum].cntVals[1].cntID;

        Glob.AgMan.InitToolPan(Glob.sceneVals[Glob.sceneNum]);

        Glob.StatPan.InitStatPans(Glob.sceneVals[Glob.sceneNum]);
        Glob.TaskPan.SetContent();

        // �������� �����
        Glob.totalMoney = 0;

        Glob.dataIsReady = true;

    }
    public void QuitBut()
    {
        Application.Quit();
    }
    public void SceneButtonClick(int scNum)
    {
        Glob.ScenePan.StopView();
        InitScene(scNum);
        if (Glob.sceneVals[scNum].autoShowScenePan)
        {
            Glob.sceneVals[scNum].autoShowScenePan = false;
            Glob.TaskPan.StartView();
        }
    }
    public scConditionsData FindInConditions(string inStr)
    {
        foreach (var scCond in Glob.sceneVals[Glob.sceneNum].conds)
        {
            if (scCond.par == inStr)
            {
                return scCond;
            }
        }
        return null;
    }
    public void ChangeMoney(int sum)
    {
        Glob.totalMoney += sum;
    }
}
