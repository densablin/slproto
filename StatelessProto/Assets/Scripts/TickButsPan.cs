using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TickButsPan : MonoBehaviour
{
    public Button StopB;
    public Button X1B;
    public Button X2B;
    public Button X3B;
    public Color deselCol = Color.red;
    public Color selCol = Color.white;
    public Image blinkImg;
    public Gradient blinkGrad;
    public float blinkStep=.05f;
    float blinkPos;

    private void Awake()
    {
        Glob.TickButsPan = this;
    }
    void Start()
    {
        Glob.DoTick += Blinker;
        ShowButs();
    }

    public void ShowButs()
    {
        X1B.image.color = deselCol;
        X2B.image.color = deselCol;
        X3B.image.color = deselCol;
        StopB.image.color = deselCol;

        switch (Glob.GMan.tickSpeed)
        {
            case GameMan.ts.stop:
                StopB.image.color = selCol;
                break;
            case GameMan.ts.X1:
                X1B.image.color = selCol;
                break;
            case GameMan.ts.X2:
                X2B.image.color = selCol;
                break;
            case GameMan.ts.X3:
                X3B.image.color = selCol;
                break;

        }
    }
    public void ButClick(int n)
    {
        Glob.GMan.tickSpeed = (GameMan.ts)n;
        ShowButs();
    }
    void Blinker()
    {
        blinkPos += blinkStep;
        blinkPos %= 1f;
        blinkImg.color = blinkGrad.Evaluate(blinkPos);
    }
}
