using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ������
// 0 - r,q+1,s-1 ->
// 1 - r-1,q+1,s \.
// 2 - r-1,q,s+1 ./
// 3 - r,q-1,s+1 <-
// 4 - r+1,q-1,s ^\
// 5 - r+1,q,s-1 /^
public class HexCell : MonoBehaviour
{
    public int myCountryID;
    public SpriteRenderer HexSpr;
    public SpriteRenderer HexSelGrid;
    public SpriteRenderer[] FrontSpr;
    public SpriteRenderer CapIconSpr;

    public int r;
    public int q;
    public int s;
    public int uId;

    public bool isInTransit;
    float trTime0; // ����� ������ ��������
    float trTime1; // ����� ��������� ��������
    public int hostCounID; // ������, ������� ������������ ������ �� ���
    public int ocupCounID; // ������, ������� ������� �������� ���������

    public bool isClick;
    float clTime0; // ����� ������ ������������ �����
    float clTime1; // ����� ��������� ������������ �����


    public bool testTransit;
    //public int uId
    //{
        // get => Glob.CalcCellId(r, q, s);
        //get => r + (q * MaxR);
    //}

    public HexCell[] NHCell = new HexCell[6];

    public HexCell Init(int inR, int inQ,int inS,int cnId)
    {
        r = inR;
        q = inQ;
        s = inS;
        uId = Glob.CalcCellId(r, q, s);
        myCountryID = cnId;

        // �������� �������������� ����� �����
        transform.localPosition = pos()
            - new Vector2(Glob.FieldH * Glob.SpaceHor, Glob.FieldV * Glob.SpaceVert) / 2;
        name = "Hex" + " " + r + "," + q + "," + s;
        gameObject.SetActive(true);

        MainSpriteSel();

        return this;
    }
    public void ResetCell()
    {
        isInTransit = false;
        myCountryID = 0;
        MainSpriteSel();
    }
    public Vector2 pos()
    {
        float x = Glob.SpaceHor * (q + r / 2f);
        float y = Glob.SpaceVert * r;
        return new Vector2(x, y);
    }
    public void UpView()
    {

        SyncFrList();

        HexSpr.color = Glob.Countries[myCountryID].cellCol;
        if (!Glob.Countries[myCountryID].isActive)
            HexSpr.color = Glob.Countries[myCountryID].cellCol * .5f;

        ShowFronts();
        ShowCapital();
    }
    void SyncFrList()
    {
        //if (Glob.Countries[myCountryID].isActive)
        //{
            if (IsFrontierCell())
            {
                if (!Glob.Countries[myCountryID].frCells.Contains(this))
                {
                    Glob.Countries[myCountryID].frCells.Add(this);
                }

            }
            else
            {
                if (Glob.Countries[myCountryID].frCells.Contains(this))
                {
                    Glob.Countries[myCountryID].frCells.Remove(this);
                }
            }
        //}
    }

    void Update()
    {


        Color baseCol = Glob.Countries[myCountryID].cellCol;
        HexSpr.color = baseCol;
        HexSelGrid.color = new Color32(255, 222, 132, 0);

        // ������������� ���� �� ������
        if (isClick)
        {
            if (Time.time >= clTime1)
            {
                isClick = false;
            }
            else
            {

                float t = Mathf.InverseLerp(clTime0, clTime1, Time.time);
                Color reCol = baseCol * 1.33f;
                float k = Glob.MField.clickCols.Evaluate(t);
                HexSpr.color = Color.Lerp(baseCol, reCol, k);
            }
        }


        // ������������� ������ ��������� ��� ������� ������ ��� ������� (� �������)
        if (Glob.AgMan.hilIsOn)
        {
            if (Glob.AgMan.hilCountID == myCountryID)
            {
                HexSelGrid.color = new Color32(255, 222, 132, 32);
                // HexSpr.color = baseCol * 1.05f;
            }
        }

        // ������������� ������ ������ ������ �������
        if (Glob.Countries[myCountryID].reAccept)
        {
            float t = Mathf.InverseLerp(Glob.Countries[myCountryID].reAcceptTime0, Glob.Countries[myCountryID].reAcceptTime1, Time.time);
            Color reCol = baseCol * 0.66f;
            float k = Glob.MField.reactCols.Evaluate(t);
            HexSpr.color = Color.Lerp(baseCol, reCol, k);
        }

        // �������� ������� ��������
        if (testTransit)
        {
            StartTransit(0);
            testTransit = false;
        }

        // ������������� ������ �������� ������
        if (isInTransit)
        {
            if (Time.time >= trTime1)
            {
                StopTransit();
            }
            else
            {
                float k = Mathf.InverseLerp(trTime0, trTime1, Time.time);
                Color trCol = Glob.MField.transCols.Evaluate(k);
                HexSpr.color = Color.Lerp(baseCol, trCol, trCol.a);
            }

        }

        // ShowCapital();
    }
    public void StartClick()
    {
        clTime0 = Time.time;
        clTime1 = Time.time + Glob.clickTime;
        isClick = true;
    }
    public void StartTransit(int inCounID)
    {
        trTime0 = Time.time;
        float k = Glob.GMan.tickSpeed switch
        {
            GameMan.ts.X1 => 1,
            GameMan.ts.X2 => 2,
            GameMan.ts.X3 => 3,
            _ => 1
        };
        trTime1 = Time.time + Glob.MField.transitTime/k;
        hostCounID = myCountryID;
        ocupCounID = inCounID;
        isInTransit = true;
        Glob.Countries[myCountryID].transCellCount ++;
        // print("Start cnt=" + myCountryID + "trCells=" + Glob.Countries[myCountryID].transCellCount);
        ShowFronts();
        Glob.GMan.ChangeMoney(Glob.GMan.cellPrice);
    }
    public void StopTransit()
    {
        Glob.Countries[myCountryID].transCellCount--;
        if (Glob.Countries[myCountryID].transCellCount < 0)
            print("Stop cnt=" + myCountryID + "trCells=" + Glob.Countries[myCountryID].transCellCount);

        if (Glob.Calc.CalcAfterTransChance(this))
        {
            Glob.Calc.AcceptTransitResults(ocupCounID, myCountryID);
            AssignToCountry(ocupCounID);
        }
        else 
        {
            Glob.Calc.AcceptTransitResults(myCountryID, ocupCounID);
        };
        isInTransit = false;
        UpView();
    }
    void ShowFronts()
    {
        for (int i = 0; i < 6; i++)
        {
            FrontSpr[i].color = new Color(1, 1, 1, 0);

            // ���� � �������� ������� ���� �� ����������
            if (isInTransit) continue;

            // ���� ��� ������ 0 (����), ������� ���� �� ����������
            if (myCountryID == 0) continue;

            // ����� � ����� ������ ������ �� ������
            if (NHCell[i] == null) continue;


            // ����� �� ����� �� ������� ������ �� ������
            if (myCountryID == NHCell[i].myCountryID) continue;

            // ����� � ������� ������ 0 (����) ������ �� ������
            if (NHCell[i].myCountryID == 0) continue;


            if (!Glob.Countries[myCountryID].isActive
                    || !Glob.Countries[NHCell[i].myCountryID].isActive)
            {
                // �� ������� � ����������� �������� ������� ����������
                FrontSpr[i].color = Glob.MField.inactiveFrontCol;
                continue;
            }

            // � ������ ���� ������ �������, ������ ������� � ����������� �� ����������
            float tense = Glob.RelateMx[myCountryID, NHCell[i].myCountryID].Tens;
            FrontSpr[i].color = Glob.MField.frontCols.Evaluate(tense);

        }
    }
    void ShowCapital()
    {
        // ������������� �������
        if (Glob.Countries[myCountryID].capitalCellId == uId)
        {
            CapIconSpr.color = Glob.MField.capIconCol;
        }
        else
        {
            CapIconSpr.color = new Color(1, 1, 1, 0);
        }
    }
    public bool IsFrontierCell()
    {
        bool isIsolate = true;
        for (int i = 0; i < 6; i++)
        {
            if (NHCell[i] != null)
            {
                if (NHCell[i].myCountryID != myCountryID)
                {
                    isIsolate = false;
                    break;
                }
            }
        }
        return !isIsolate;
    }
    public bool HasFrWithCnID(int cnId)
    {
        for (int i = 0; i < 6; i++)
        {
            if (NHCell[i] != null)
            {
                // print("hex=" + gameObject.name + " i=" + i + " NH=" + NHCell[i].myCountryID);
                if (NHCell[i].myCountryID == cnId)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public HexCell GetRandomNHCell(int otherCnID) // �������� ��������� ������ ������, �� ������ ��� ������
    {
        List<HexCell> needNH = new List<HexCell>();
        for (int i = 0; i < 6; i++)
        {
            if (NHCell[i] != null)
            {
                if (NHCell[i].myCountryID == otherCnID) needNH.Add(NHCell[i]);
            }
        }
        if (needNH.Count > 0)
        {
            int r = Random.Range(0, needNH.Count);
            return needNH[r];
        }
        else
        {
            print("cant find needNH of hc=" + gameObject.name + "of cnID=" + otherCnID);
            return null;
        }
    }
    public int HowManyNHCells(int otherCntID)
    {
        int n = 0;
        foreach (var nh in NHCell)
        {
            if (nh != null)
            {
                if (nh.myCountryID == otherCntID) n++;
            }
        }
        return n;
    }
    public void AssignToCountry(int cnId)
    {
        if (myCountryID !=cnId) // ���� ������ ������������� ����������
        {
            int oldCountryCnId = myCountryID;
            myCountryID = cnId;

            // ������� �� ���������� ������
            try
            {
                Glob.Countries[oldCountryCnId].myCells.Remove(this);
                Glob.Countries[oldCountryCnId].frCells.Remove(this);
            }
            catch (System.Exception)
            {
                print("cant remove cell=" + gameObject.name + " �� ������=" + oldCountryCnId);
                throw;
            }

            // � �������� � ������ ������
            Glob.Countries[myCountryID].myCells.Add(this);
        } else
        {
            print("double assing hc=" + name + " cnt=" + myCountryID);
            if (!Glob.Countries[myCountryID].myCells.Contains(this))
            {
                Glob.Countries[myCountryID].myCells.Add(this);
                print("but wasnt contain");
            }
        }

        foreach (var nh in NHCell)
        { 
            if(nh != null) nh.UpView();
        }

        
        MainSpriteSel();
        UpView();
    }
    public void AssignToCountrySimple(int cnId)
    {
        myCountryID = cnId;

        Glob.Countries[myCountryID].myCells.Add(this);
        if (IsFrontierCell())
            Glob.Countries[myCountryID].frCells.Add(this);

        MainSpriteSel();
        UpView();
    }
    public void MainSpriteSel()
    {

        HexSpr.sprite = Glob.MField.GetRandomHCellSprite(myCountryID);
        HexSpr.transform.eulerAngles = new Vector3(0, 0, Glob.MField.GetRandomHCellAngle(myCountryID));

        return;
    }

}
